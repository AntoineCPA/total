﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Configuration;

namespace CPA.EMEA.TOTAL.DocDispatch.BU
{
    public delegate void dlWriteMsgDelegate(string msg, params object[] param);

    public class Business
    {
        public static char separator;
        public static string inputFolder;
        public static string errorFolder;
        public static string targetFolder;
        public static MessageLevel Current_msgLevel = MessageLevel.error;
        public static string LogFile;

        public Business()
        {
            separator = ConfigurationManager.AppSettings["separator"][0] ;
            inputFolder = ConfigurationManager.AppSettings["inputFolder"];
            errorFolder = ConfigurationManager.AppSettings["errorFolder"];
            targetFolder = ConfigurationManager.AppSettings["targetFolder"];

            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["MessageLevel"]) &&
                string.Compare(ConfigurationManager.AppSettings["MessageLevel"],"info",true) == 0 )
                Current_msgLevel = MessageLevel.info ;

            LogFile = ConfigurationManager.AppSettings["LogFile"];


        }

        public dlWriteMsgDelegate msgCusto;

        public void WriteMsg(MessageLevel msgLevel, string msg, params object[] param)
        {
            if (msgCusto != null)
                msgCusto("BU:" + msg, param);

            if (Current_msgLevel == MessageLevel.info || msgLevel == MessageLevel.error)
            {
                if (param == null || param.Count() == 0)
                {
                    msg = msg.Replace("{", "{{").Replace("}", "}}");
                }

                DataAccessHelper.LogFile(msgLevel, string.Format(msg, param));
            }
        }

        /// <summary>
        /// Full TargetPath, create if not exist, is looking of directories name before "_"
        /// </summary>
        /// <param name="subTargetDirectory"></param>
        /// <returns></returns>
        public string CreateDirectories(string subTargetDirectory)
        {
            string FullPath = System.IO.Path.Combine(targetFolder, subTargetDirectory);
            try {
                //Optimisation :
                

                if (System.IO.Directory.Exists(FullPath))
                {
                    return FullPath;
                }

                StringBuilder FullRealPath = new StringBuilder(targetFolder.TrimEnd('\\'));

                for (int i = 0; i < subTargetDirectory.Split('\\').Length; i++)
                {
                    string Current = subTargetDirectory.Split('\\')[i];

                    if (string.IsNullOrEmpty(Current))
                        continue;

                    if (Directory.Exists(Path.Combine(FullRealPath.ToString(), Current)))
                        FullRealPath.AppendFormat(@"\{0}", Current);
                    else
                    {
                        if (Directory.GetDirectories(FullRealPath.ToString(), Current + "_*").Count() > 0)
                        {
                            string newCurrent = Directory.GetDirectories(FullRealPath.ToString(), Current + "_*").First();
                            FullRealPath.Clear();
                            FullRealPath.Append(newCurrent);
                        }
                        else
                        {
                            // WriteMsg(MessageLevel.info, "Create Directory :{0}", FullRealPath.ToString());
                            FullRealPath.AppendFormat(@"\{0}", Current);
                            System.IO.Directory.CreateDirectory(FullRealPath.ToString());
                        }
                    }
                }


                return FullRealPath.ToString();
            }catch(Exception ex)
            {
                return string.Format("Unable to create {0}, Error : {1}.", FullPath, ex.Message);
            }

        }



        /// <summary>
        /// 
        /// </summary>
        public void Moulinette()
        {
            WriteMsg(MessageLevel.info, "Start Moulinette");

            DataAccessHelper dataAccessHelper = new DataAccessHelper();

            string[] FilestoTODO = System.IO.Directory.GetFiles(inputFolder);

            WriteMsg(MessageLevel.info, @"Moulinette : {0} files to proceed", FilestoTODO.Count());

            // make the process !!!
            foreach (string fullFilePath in FilestoTODO)
            {
                string fileName = System.IO.Path.GetFileName(fullFilePath);
                try {

                    #region ExCode

    //                string fileName = System.IO.Path.GetFileName(fullFilePath);
    //                try
    //                {

    //                    //Exemple for file name :
    //                    // "20161017$Sender Name$CASE REFERENCE$document name.pdf"

    //                    WriteMsg(MessageLevel.info, "Start fileName: {0}", fileName);

    //                    //TODO
    //                    //test format.
    //                    if (!System.Text.RegularExpressions.Regex.IsMatch(fileName, @"2[0-9][0-9][0-9][0-1][0-9][0-3][0-9]\$.*\$.*\$.*\..*"))
    //                    {
    //                        throw new Exception(string.Format("File name :{0} doesn't match", fileName));
    //                    }

    //                    string date = fileName.Split(separator)[0];
    //                    string senderName = fileName.Split(separator)[1];
    //                    string casereference = fileName.Split(separator)[2];
    //                    string DocName = fileName.Split(separator)[3];

    //                    DateTime dtFile = DateTime.ParseExact(date, @"yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);



    //                    // Call DB to have the filePath
    //                    string tempFilePath = dataAccessHelper.GetFilePathFromFileName(fileName, date);


    //                    //Create file directories if needed
    //                    string RealFilePath = CreateDirectories(tempFilePath);

    //                    string newFileName = string.Format(@"{0}-{1}-{2}-{3}",
    //dtFile.ToString("yyyy-MM-dd"),
    //senderName,
    //casereference,
    //DocName
    //);
    //                    string Destination = System.IO.Path.Combine(RealFilePath, newFileName);

    //                    WriteMsg(MessageLevel.info, $"Move \"{fullFilePath}\" to \"{Destination}\" ");

    //                    if (System.IO.File.Exists(Destination))
    //                        throw new Exception(string.Format("File name :{0} already exist.", newFileName));

    //                    //Add the document to Memotech via DMS
    //                    string documentKey = dataAccessHelper.AddDocInMemotechDMS(casereference, DocName, fullFilePath);
    //                    if (string.IsNullOrEmpty(documentKey))
    //                        throw new Exception($"Unable to insert doc: \"{DocName}\" for case: \"{casereference}\".");


    //                    System.IO.File.Move(fullFilePath, Destination);

    //                    WriteMsg(MessageLevel.info, $"End of Process for \"{fullFilePath}\" to \"{Destination}\" ");

    //                }
    //                catch (Exception ex)
    //                {
    //                    WriteMsg(MessageLevel.error, @"Error processing ""{0}"", Error msg:{1}", fullFilePath, ex.Message);

    //                    //If error file already in error directory !
    //                    string targetErrorFilePath = System.IO.Path.Combine(errorFolder, fileName);
    //                    if (System.IO.File.Exists(targetErrorFilePath))
    //                    {
    //                        string tempDir = DateTime.Now.Ticks.ToString();

    //                        string NewErrorFilePath = System.IO.Path.Combine(errorFolder, tempDir);
    //                        System.IO.Directory.CreateDirectory(NewErrorFilePath);
    //                        System.IO.File.Move(fullFilePath, System.IO.Path.Combine(NewErrorFilePath, fileName));
    //                    }
    //                    else
    //                    {
    //                        System.IO.File.Move(fullFilePath, targetErrorFilePath);
    //                    }
    //                }

                    #endregion

                    //Exemple for file name :
                    // "20161017-Sender Name-CASE REFERENCE-document name.pdf"

                    WriteMsg(MessageLevel.info, "Start fileName: {0}", fileName);

                    DateTime dtCreationFile = System.IO.File.GetCreationTime(fullFilePath);

                    List<string> retour = dataAccessHelper.GetFilePathFromFileName(fileName, dtCreationFile);

                    string newFilePath = retour[0];
                    string newFileName = retour[1];
                    string CaseKey = retour[2];
                    string FolderKey = retour[3];
                    string ErrorMessage = retour[4];

                    if(!string.IsNullOrEmpty(ErrorMessage))
                    {
                        throw new Exception(ErrorMessage);
                    }

                    WriteMsg(MessageLevel.info, $"newFilePath: \"{newFilePath}\"; \newFileName: \"{newFileName}\"; CaseKey: \"{CaseKey}\"; FolderKey: \"{FolderKey}\"; ErrorMessage: \"{ErrorMessage}\";");

                    //Create file directories if needed
                    string RealFilePath = CreateDirectories(newFilePath);
                    string Destination = System.IO.Path.Combine(RealFilePath, newFileName);

                    

                    if(System.IO.File.Exists(Destination))
                        throw new Exception(string.Format("File name :{0} already exist.", newFileName));

                    WriteMsg(MessageLevel.info, $"Call DMS \"{fullFilePath}\" to CaseKey: \"{CaseKey}\"; FolderKey: \"{FolderKey}\".");
                    
                    //Add the document to Memotech via DMS
                    //string documentKey = dataAccessHelper.AddDocInMemotechDMS(CaseKey, FolderKey,System.IO.Path.GetFileName(newFileName), fullFilePath);

                    string documentKey = dataAccessHelper.AddDocInMemotechDMS(CaseKey, FolderKey, newFileName, fullFilePath);

                    if (string.IsNullOrEmpty(documentKey))
                        throw new Exception($"Unable to insert doc: \"{newFileName}\" for casekey: \"{CaseKey}\".");

                    WriteMsg(MessageLevel.info, $"Move \"{fullFilePath}\" to \"{Destination}\" ");
                    System.IO.File.Move(fullFilePath, Destination);
                     

                    WriteMsg(MessageLevel.info, $"End of Process for \"{fullFilePath}\" to \"{Destination}\" ");

                }
                catch(Exception ex)
                {
                    WriteMsg(MessageLevel.error, @"Error processing ""{0}"", Error msg:{1}", fullFilePath,ex.Message  );

                    //If error file already in error directory !
                    string targetErrorFilePath = System.IO.Path.Combine(errorFolder, fileName);
                    if (System.IO.File.Exists(targetErrorFilePath))
                    {
                        string tempDir = DateTime.Now.Ticks.ToString() ;

                        string NewErrorFilePath = System.IO.Path.Combine(errorFolder, tempDir);
                        System.IO.Directory.CreateDirectory(NewErrorFilePath);
                        System.IO.File.Move(fullFilePath, System.IO.Path.Combine(NewErrorFilePath, fileName)) ;
                    }
                    else
                    {
                        System.IO.File.Move(fullFilePath, targetErrorFilePath);
                    }
                }
            }

            WriteMsg(MessageLevel.info, "End Moulinette");
        }
    }
}
