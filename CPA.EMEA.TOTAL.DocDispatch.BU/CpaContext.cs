﻿using CPAmemotech.Dms.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CPA.EMEA.TOTAL.DocDispatch.BU
{
    /// <summary>
    /// Define the information needed for security checking over WCF service calls.
    /// The instance of this class will be sent as Custom header to each WCF call on client side. 
    /// Service implementation can use this information in order to check security.
    /// </summary>
    public class CpaContext : IClientMessageInspector
    {

        #region IClientMessageInspector Members
        /// <summary>
        /// Enables inspection or modification of a message after a reply message is
        ///     received but prior to passing it back to the client application.
        /// </summary>
        /// <param name="reply">The message to be transformed into types and handed back to the client application.</param>
        /// <param name="correlationState">Correlation state data.</param>
        public void AfterReceiveReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
        {
        }

        /// <summary>
        /// Enables inspection or modification of a message before a request message is sent to a service.
        /// </summary>
        /// <param name="request">The message to be sent to the service.</param>
        /// <param name="channel">The WCF client object channel.</param>
        /// <returns></returns>
        public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
        {
            if (request.Headers.FindHeader("CpaContext", "CpaMemotech") < 0)
            {
                string[] cpaCtx = {"administrator",
                         "1",
                         "en-GB"};

                MessageHeader<string[]> msg = new MessageHeader<string[]>(cpaCtx);

                msg.Actor = string.Empty;
                MessageHeader untyped = msg.GetUntypedHeader("CpaContext", "CpaMemotech");
                request.Headers.Add(untyped);
            }
            return null;
        }

        #endregion
    }


    public class MyBehavior : IEndpointBehavior
    {
        public void AddBindingParameters(ServiceEndpoint endpoint, BindingParameterCollection bindingParameters)
        {
            //no-op
        }

        public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
        {
            CpaContext cpaContext = new CpaContext();
            clientRuntime.MessageInspectors.Add(cpaContext);
        }

        public void ApplyDispatchBehavior(ServiceEndpoint endpoint, EndpointDispatcher endpointDispatcher)
        {
            //no-op
        }

        public void Validate(ServiceEndpoint endpoint)
        {
            //no-op
        }
    }

}



