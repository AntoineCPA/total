﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cpa.Dms.Contracts.Services;
using System.ServiceModel;


namespace CPA.EMEA.TOTAL.DocDispatch.BU
{
    /// <summary>
    /// 
    /// </summary>
    public class DMSService : System.ServiceModel.ClientBase<IDmsService>, IDisposable
    {

        public DMSService()
        {

            // public object BeforeSendRequest(ref System.ServiceModel.Channels.Message request, IClientChannel channel)
            //{
            //  if (request.Headers.FindHeader("CpaContext", "CpaMemotech") < 0)
            //  {

            //    string[] cpaCtx = {Thread.CurrentPrincipal.Identity.Name, 
            //                     Thread.CurrentPrincipal is CpaPrincipal ? ((CpaPrincipal)Thread.CurrentPrincipal).UserIdentityKey :"",
            //                     Thread.CurrentThread.CurrentUICulture.ToString()};

            //    MessageHeader<string[]> msg = new MessageHeader<string[]>(cpaCtx);

            //    msg.Actor = string.Empty;
            //    MessageHeader untyped = msg.GetUntypedHeader("CpaContext", "CpaMemotech");
            //    request.Headers.Add(untyped);
            //  }
            //  return null;

        }

        public DMSService(string endpointConfigurationName) : base(endpointConfigurationName)
        {
        }

        public DMSService(string endpointConfigurationName, string remoteAddress) : base(endpointConfigurationName, remoteAddress)
        {
        }

        public DMSService(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
            base(endpointConfigurationName, remoteAddress)
        {
        }

        public DMSService(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
            base(binding, remoteAddress)
        {
        }

        public void OpenService()
        {
            base.Open();
        }

        public IDmsService Operations()
        {
            return base.Channel;
        }

        void IDisposable.Dispose()
        {
            base.Close();
        }
    }
}


