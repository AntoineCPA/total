﻿using System;
using CPA.BusinessFacade;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Configuration;
using System.Transactions;
using System.IO;

namespace CPA.EMEA.TOTAL.DocDispatch.BU
{
    public enum MessageLevel
    {
        info,
        error
    };

    public class DataAccessHelper
    {
        /// <summary>
        /// For test
        /// </summary>
        /// <returns></returns>
        public string GetCurrentDate()
        {

            try
            {
                //LocalSetting.WritMsg("Ds {0}", System.Reflection.MethodBase.GetCurrentMethod().Name);
                string result = string.Empty;

                //string procName = string.Format("GTW_INSERM_GRC.GetCurrentDate", System.Reflection.MethodBase.GetCurrentMethod().Name);
                string procName = @"TOTALGED.GetCurrentDate";

                DataSet dt = CaseManagement.WebLinkDbCall(
                       new CPA.Common.Security.UserIdentity(LocalSetting.WS_USERIDENTITYKEY, "Windows Service Administrator"),
                       "en-US",
                       "defaultConnection",
                       procName,
                       new Hashtable());

                result = dt.Tables[0].Rows[0][0].ToString();

                return result;
            }
            catch (Exception ex)
            {
                // Log
                LogFile(MessageLevel.error, string.Format("Error in {0}, Msg : {1}.{2}", System.Reflection.MethodInfo.GetCurrentMethod(), ex.Message, ex.InnerException != null ? "\r\n InnerException: " + ex.InnerException : "")) ;
                throw;
                
            }
        }

        public List<string> GetFilePathFromFileName(string FileName,DateTime date)
        {

            List<string> retour = new List<string>();
            try
            {
                
                string procName = @"TOTALGED.GetFilePathFromFileName";

                Hashtable htpars = new Hashtable();
                htpars.Add("FileName", FileName);
                htpars.Add("FileDate", date.ToString("yyyyMMdd"));

                DataSet dt = CaseManagement.WebLinkDbCall(
                       new CPA.Common.Security.UserIdentity(LocalSetting.WS_USERIDENTITYKEY, "Windows Service Administrator"),
                       "en-US",
                       "defaultConnection",
                       procName,
                       htpars
                       );

                //strFilePath AS FilePath,
                //strFileName AS FileName,
                //intCaseKey AS CaseKey,
                //intFolderKey AS FolderKey,
                //NULL AS ErrorMessage

                retour.Add(dt.Tables[0].Rows[0][0] as string);
                retour.Add(dt.Tables[0].Rows[0][1] as string);
                retour.Add(dt.Tables[0].Rows[0][2] as string);
                retour.Add(dt.Tables[0].Rows[0][3] as string);
                retour.Add(dt.Tables[0].Rows[0][4] as string);

                return retour;
            }
            catch (Exception ex)
            {
                // Log
                LogFile(MessageLevel.error, string.Format("Error in {0}, Msg : {1}.{2}", System.Reflection.MethodInfo.GetCurrentMethod(), ex.Message, ex.InnerException != null ? "\r\n InnerException: " + ex.InnerException : ""));
                throw;

            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="CaseReference"></param>
        /// <returns>CaseKey; FolderKey</returns>
        public string[] GetCaseFolderKey(string CaseReference)
        {
            string[] retour = new string[2] ;
            try
            {
                string procName = @"TOTALGED.GetCaseFolderKey";

                Hashtable hs = new Hashtable();
                hs.Add("caseRef" ,CaseReference);

                DataSet dt = CaseManagement.WebLinkDbCall(
                       new CPA.Common.Security.UserIdentity(LocalSetting.WS_USERIDENTITYKEY, "Windows Service Administrator"),
                       "en-US",
                       "defaultConnection",
                       procName,
                       hs);


                if(dt.Tables[0].Rows[0][0] == null)
                {
                    throw new Exception($"No {CaseReference} or folder in Memotech DBB");
                }

                retour[0] = dt.Tables[0].Rows[0][0].ToString() ;
                retour[1] = dt.Tables[0].Rows[0][1].ToString() ;

            }
            catch (Exception ex)
            {
                // Log
                LogFile(MessageLevel.error, string.Format("Error in {0}, Msg : {1}.{2}", System.Reflection.MethodInfo.GetCurrentMethod(), ex.Message, ex.InnerException != null ? "\r\n InnerException: " + ex.InnerException : ""));
                throw;

            }
            return retour;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns>file Path</returns>
        public string GetFilePathFromFileName(string FileName,string FileDate)
        {

            try
            {
                string result = string.Empty;
                string procName = @"TOTALGED.GetFilePathFromFileName";

                Hashtable htpars  = new Hashtable();
                htpars.Add("FileName", FileName);
                htpars.Add("FileDate", FileDate);

                DataSet dt = CaseManagement.WebLinkDbCall(
                       new CPA.Common.Security.UserIdentity(LocalSetting.WS_USERIDENTITYKEY, "Windows Service Administrator"),
                       "en-US",
                       "defaultConnection",
                       procName,
                       htpars
                       );

                result = dt.Tables[0].Rows[0][0].ToString();

                return result;
            }
            catch (Exception ex)
            {
                // Log
                LogFile(MessageLevel.error, string.Format("Error in {0}, Msg : {1}.{2}", System.Reflection.MethodInfo.GetCurrentMethod(), ex.Message, ex.InnerException != null ? "\r\n InnerException: " + ex.InnerException : ""));
                throw;

            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="msgLevel"></param>
        /// <param name="msg"></param>
        public static void LogFile(MessageLevel msgLevel, string msg)
        {
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["LogFile"]))
            {
                System.IO.File.AppendAllText(ConfigurationManager.AppSettings["LogFile"], DateTime.Now.ToString("yyMMdd-HH:mm:ss") + "\t" + msgLevel.ToString() + "\t" + msg + "\r\n");
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="casekey"></param>
        /// <param name="docName"></param>
        /// <param name="sysDocfilePath"></param>
        /// <returns></returns>
        public string AddDocInMemotechDMS(string casekey,string FolderKey,string docName, string sysDocfilePath)
        {
            //string[] temp = GetCaseFolderKey(CaseReference);

            //string casekey = temp[0] ;
            //string FolderKey = temp[1] ;

            string keyDocCreated;
            string filepathname = sysDocfilePath ;

            try
            {

                Cpa.Dms.Contracts.Data.Document tpDoc = new Cpa.Dms.Contracts.Data.Document();
                tpDoc.Title = System.IO.Path.GetFileNameWithoutExtension(docName) ;

                tpDoc.Archived = false;
                tpDoc.Authors = "Jhon Doe";

                tpDoc.File = new Cpa.Dms.Contracts.Data.DocumentFile();

                tpDoc.File.FileName = docName ;
                tpDoc.File.MimeType = GetMimeType(System.IO.Path.GetExtension(docName)) ;
                
                using (FileStream file = new FileStream(filepathname, FileMode.Open, FileAccess.Read))
                {
                    tpDoc.File.BinaryContent = new byte[file.Length];
                    file.Read(tpDoc.File.BinaryContent, 0, (int)file.Length);

                }

                tpDoc.File.Size = tpDoc.File.BinaryContent.LongLength;
                tpDoc.Metadata = new System.Collections.ObjectModel.Collection<Cpa.Dms.Contracts.Data.DmsMetadata>();

                Cpa.Dms.Contracts.Data.DmsMetadata meta = new Cpa.Dms.Contracts.Data.DmsMetadata();
                meta.PropertyName = "MediaTypeKey";
                meta.PropertyValue = "550";
                tpDoc.Metadata.Add(meta);

                meta = new Cpa.Dms.Contracts.Data.DmsMetadata();
                meta.PropertyName = "IsUniqueMedia";
                meta.PropertyValue = true;
                tpDoc.Metadata.Add(meta);

                meta = new Cpa.Dms.Contracts.Data.DmsMetadata();
                meta.PropertyName = "FolderContentKey";
                meta.PropertyValue = "NewServerKey0000000016";
                tpDoc.Metadata.Add(meta);

                tpDoc.Reference = docName ;
                tpDoc.Subject = "";
                tpDoc.Comments = "";

                tpDoc.Answers = new System.Collections.ObjectModel.Collection<Cpa.Dms.Contracts.Data.Document>();
                tpDoc.Answers.Clear();

                tpDoc.OwnerEntity = new Cpa.Dms.Contracts.Data.BusinessEntity();
                // TODO casekey 
                // for F-1318  => 6299713
                tpDoc.OwnerEntity.EntityKey = casekey;

                tpDoc.Created = DateTime.Now;
                tpDoc.Modified = DateTime.Now;

                tpDoc.ParentFolder = new Cpa.Dms.Contracts.Data.Folder();

                // TODO folderkey  
                // foder  F-1318  => 23704301
                tpDoc.ParentFolder.FolderKey = FolderKey;

                tpDoc.DocumentKey = CPA.EMEA.TOTAL.DocDispatch.BU.LocalSetting.GetNextKeyPrefix();

                using (TransactionScope trScope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions(), EnterpriseServicesInteropOption.Automatic))
                {
                    using (DMSService service = new DMSService("ThirdPartyDMS"))
                    {
                        service.Endpoint.EndpointBehaviors.Add(new MyBehavior());
                        service.Open();

                        Cpa.Dms.Contracts.Data.Document tpDocRetour = service.Operations().AddDocument(tpDoc);

                        keyDocCreated = tpDocRetour.DocumentKey;
                    }

                    trScope.Complete();
                }

            }
            catch (Exception ex)
            {
                // Log
                LogFile(MessageLevel.error, string.Format("Error in {0}, Msg : {1}.{2}", System.Reflection.MethodInfo.GetCurrentMethod(), ex.Message, ex.InnerException != null ? "\r\n InnerException: " + ex.InnerException : ""));
                throw;

            }
            return keyDocCreated;
        }


        private string GetMimeType(string fileExtension)
        {
            if (string.Compare(fileExtension, "txt", true) == 0)
                return @"text/plain";

            if (string.Compare(fileExtension, "pdf", true) == 0)
                return @"application/pdf";
            if (string.Compare(fileExtension, "doc", true) == 0 || string.Compare(fileExtension, "docx", true) == 0)
                return @"application/msword";

            if (string.Compare(fileExtension, "xls", true) == 0 || string.Compare(fileExtension, "xlsx", true) == 0)
                return @"application/ms-excel";

            if (string.Compare(fileExtension, "msg", true) == 0)
                return @"application/vnd.ms-outlook";

            if (string.Compare(fileExtension, "ppt", true) == 0)
                return "application/vnd.ms-powerpoint";

            return @"octet/stream";

        }

    }
}
