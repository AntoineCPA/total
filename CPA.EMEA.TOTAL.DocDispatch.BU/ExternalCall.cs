﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPA.EMEA.TOTAL.DocDispatch.BU
{
    public class ExternalCall
    {
        /// <summary>
        /// Create to be called from aspx page.
        /// </summary>
        /// <param name="subTargetDirectory"></param>
        /// <returns></returns>
        public static string GetFilePath(string subTargetDirectory)
        {
            // TO test:
            //return "subTargetDirectory:" + subTargetDirectory+".";
            try
            {
                Business bu = new Business();

                return bu.CreateDirectories(subTargetDirectory);

            }catch(Exception ex)
            {

                return string.Format("Error: {0}.",ex.Message) ;
                    
            }


        }

    }
}
