CREATE OR REPLACE PACKAGE TOTALGED AS

  FUNCTION GetFilePathFromCase (psCaseReference IN VARCHAR2) RETURN VARCHAR2;
  PRAGMA RESTRICT_REFERENCES (GetFilePathFromCase,WNDS,WNPS);

  PROCEDURE GetCurrentDate (psUserIdentityKey   IN  VARCHAR2,
                            psCulture           IN  VARCHAR2,
                            psXmlParameters     IN  VARCHAR2,
                            pcResult            OUT CPAmemotechType.RefCur);

  PROCEDURE GetFilePathFromFileName (psUserIdentityKey   IN  VARCHAR2,
                                     psCulture           IN  VARCHAR2,
                                     psXmlParameters     IN  VARCHAR2,
                                     pcResult            OUT CPAmemotechType.RefCur);

END TOTALGED;
/