/* Formatted on 16/11/2016 17:38:04 (QP5 v5.227.12220.39754) */
CREATE OR REPLACE PACKAGE BODY TOTAL.TOTALGED
AS
   strSeparator   CONSTANT CHAR (1) := '-';

   PROCEDURE GetCurrentDate (psUserIdentityKey   IN     VARCHAR2,
                             psCulture           IN     VARCHAR2,
                             psXmlParameters     IN     VARCHAR2,
                             pcResult               OUT CPAmemotechType.RefCur)
   IS
   BEGIN
      OPEN pcResult FOR SELECT SYSDATE FROM DUAL;
   END GetCurrentDate;

   PROCEDURE LogTest (psDescription VARCHAR2 DEFAULT NULL)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
      psOwner        VARCHAR2 (256);
      psName         VARCHAR2 (256);
      pnLineNo       NUMBER;
      psCallerType   VARCHAR2 (256);
   BEGIN
      INSERT INTO APPLICATIONLOG (LOGKEY,
                                  OWNERNAME,
                                  OBJECTNAME,
                                  DESCRIPTION,
                                  LOGDATE,
                                  PROCESSKEY,
                                  JOBKEY,
                                  TARGETREFERENCE)
           VALUES (SysAudit.NEXTVAL,
                   USER,
                   'TOTALGED',
                   psDescription,
                   SYSDATE,
                   0,
                   100,
                   '-');

      COMMIT;
   END LogTest;

   PROCEDURE GetCaseFolderKey (psUserIdentityKey   IN     VARCHAR2,
                               psCulture           IN     VARCHAR2,
                               psXmlParameters     IN     VARCHAR2,
                               pcResult               OUT CPAmemotechType.RefCur)
   IS
      xmlData        XMLTYPE;
      intCaseKey     INTEGER;
      strCaseRef     VARCHAR2 (256);
      intNbCases     INTEGER;
      intFolderKey   INTEGER;
      -- Error Management
      Err_Cod        INTEGER;
      Err_Msg        VARCHAR2 (1024);
      Err_Zone       VARCHAR2 (1024);
   BEGIN
      BEGIN
         xmlData := xmltype (psXmlParameters);

         SELECT TRIM (EXTRACTVALUE (xmlData, '/parameters/parameter[@name="CaseRef"]')) INTO strCaseRef FROM DUAL;

         SELECT MIN (CaseKey), COUNT (CaseKey)
           INTO intCaseKey, intNbCases
           FROM Cases
          WHERE Cases.CaseReference LIKE strCaseRef || '%';

         IF intNbCases <> 1
         THEN
            RAISE_APPLICATION_ERROR (-20000, 'Cannot find Case Reference : ' || strCaseRef);
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            Err_Cod := SQLCODE;
            Err_Msg := SUBSTR (SQLERRM (Err_Cod), 1, 250);
            LogTest ('Error ' || Err_Cod || ' ,  ' || Err_Msg);
            intCaseKey := -1;
            ROLLBACK;
      END;

      OPEN pcResult FOR SELECT intCaseKey AS "CASEKEY", intFolderKey AS "FOLDERKEY" FROM DUAL;
   END GetCaseFolderKey;

   PROCEDURE GetFilePathFromFileName (psUserIdentityKey   IN     VARCHAR2,
                                      psCulture           IN     VARCHAR2,
                                      psXmlParameters     IN     VARCHAR2,
                                      pcResult               OUT CPAmemotechType.RefCur)
   IS
      xmlData              XMLTYPE;
      psFileName           VARCHAR2 (1024);
      psFileDate           VARCHAR2 (8);
      intCaseKey           INTEGER;
      strCaseRef           VARCHAR2 (50);
      strLevel1            VARCHAR2 (50);
      strLevel2            VARCHAR2 (50);
      strLevel3            VARCHAR2 (50);
      strLevel4            VARCHAR2 (50);
      strFilePath          VARCHAR2 (1024);
      strFileName          VARCHAR2 (1024);
      strFileDate          VARCHAR2 (10);
      intFolderKey         INTEGER;
      intNbCases           INTEGER;
      intPos1              INTEGER;
      intPos2              INTEGER;
      -- Oracle errors
      OracleErrorCode      INTEGER;
      OracleErrorMessage   VARCHAR2 (500);
   BEGIN
      xmlData := xmltype (psXmlParameters);

      SELECT EXTRACTVALUE (xmlData, '/parameters/parameter[@name="FileName"]'), EXTRACTVALUE (xmlData, '/parameters/parameter[@name="FileDate"]')
        INTO psFileName, psFileDate
        FROM DUAL;

      -- Extract date
      intPos1 :=
         INSTR (psFileName,
                strSeparator,
                1,
                1);

      IF intPos1 = 1
      THEN
         strFileDate := NVL (psFileDate, TO_CHAR (SYSDATE, 'YYYYMMDD'));
      ELSIF intPos1 = 9
      THEN
         strFileDate := SUBSTR (psFileName, 1, intPos1 - 1);
      ELSE
         RAISE_APPLICATION_ERROR (-20200, '01. Invalid date');
      END IF;

      strFileDate := TO_CHAR (TO_DATE (strFileDate, 'YYYYMMDD'), 'YYYY-MM-DD');
      strFileName := strFileDate;
      -- Extract Mail From
      intPos1 :=
         INSTR (psFileName,
                strSeparator,
                1,
                1);
      intPos2 :=
         INSTR (psFileName,
                strSeparator,
                1,
                2);
      strFileName := strFileName || SUBSTR (psFileName, intPos1, intPos2 - intPos1);
      -- Extract Case Reference
      intPos1 :=
         INSTR (psFileName,
                strSeparator,
                1,
                2);
      intPos2 :=
         INSTR (psFileName,
                strSeparator,
                -1,
                1);

      IF intPos1 = 0 OR intPos2 = 0
      THEN
         RAISE_APPLICATION_ERROR (-20200, '10. Case Reference cannot be identified');
      END IF;

      strCaseRef := SUBSTR (psFileName, intPos1 + 1, intPos2 - intPos1 - 1);
      strFileName := strFileName || '-' || strCaseRef;
      -- Extract Case Reference'
      intPos1 :=
         INSTR (psFileName,
                strSeparator,
                -1,
                1);
      strFileName := strFileName || SUBSTR (psFileName, intPos1);
      -- Calculate path to store document on file server
      strCaseRef := REPLACE (strCaseRef, '/', ' ');

      SELECT MIN (CaseKey), COUNT (CaseKey)
        INTO intCaseKey, intNbCases
        FROM Cases
       WHERE Cases.CaseTypeKey != 50 AND Cases.CaseReference = strCaseRef;

      IF intNbCases = 0
      THEN
         SELECT MIN (CaseKey), COUNT (CaseKey)
           INTO intCaseKey, intNbCases
           FROM Cases
          WHERE Cases.CaseTypeKey != 50 AND Cases.CaseReference LIKE strCaseRef || '%';
      END IF;

      IF intNbCases = 0
      THEN
         RAISE_APPLICATION_ERROR (-20200, '11. No Case Reference match (' || strCaseRef || ')');
      ELSIF intNbCases > 1
      THEN
         intCaseKey := NULL;
         RAISE_APPLICATION_ERROR (-20200, '12. Multiple Case References match (' || strCaseRef || ')');
      END IF;

      intPos1 := INSTR (strCaseRef, '-');
      intPos2 := INSTR (strCaseRef, ' ');

      IF intPos1 = 0 AND intPos2 = 0
      THEN
         strLevel3 := strCaseRef;
         strLevel4 := NULL;
      ELSE
         IF intPos2 < intPos1 AND intPos2 > 0
         THEN
            intPos1 := INSTR (strCaseRef, ' ');
         END IF;

         intPos2 :=
            INSTR (strCaseRef,
                   '-',
                   -1,
                   2);

         IF intPos2 = 0 OR intPos2 <= intPos1
         THEN
            strLevel3 := strCaseRef;
         ELSE
            strLevel3 := SUBSTR (strCaseRef, 1, intPos2 - 1);
            strLevel4 := strCaseRef;
         END IF;
      END IF;

      intPos1 := INSTR (REPLACE (strLevel3, ' ', '-'), '-');
      intPos2 :=
         INSTR (REPLACE (strLevel3, ' ', '-'),
                '-',
                1,
                2);

      IF intPos2 = 0
      THEN
         strLevel2 := strLevel3;
      ELSE
         strLevel2 := SUBSTR (strLevel3, 1, intPos2 - 1);
      END IF;

      IF intPos1 > 0
      THEN
         IF SUBSTR (strLevel2, 1, intPos1 - 1) = RTRIM (TRANSLATE (SUBSTR (strLevel2, 1, intPos1 - 1), '0123456789', '9999999999'), '9')
         THEN
            strLevel2 := SUBSTR (strLevel2, 1, LENGTH (strLevel2) - 2) || '00';
         ELSE
            strLevel2 := SUBSTR (strLevel2, 1, intPos1 - 1);
         END IF;
      END IF;

      strLevel1 := RTRIM (TRANSLATE (strLevel2, ' -0123456789', '999999999999'), '9');
      strFilePath := strLevel1 || '\' || strLevel2 || '\' || strLevel3 || '\';                                                                                                                                                                                                                                                                                                                               --'

      IF strLevel4 IS NOT NULL
      THEN
         strFilePath := strFilePath || strLevel4 || '\';                                                                                                                                                                                                                                                                                                                                                     --'
      END IF;

      -- Identify folderKey
      SELECT MIN (Folder.FolderKey)
        INTO intFolderKey
        FROM Folder
       WHERE Folder.CaseKey = intCaseKey;

      IF intFolderKey = 0
      THEN
         RAISE_APPLICATION_ERROR (-20200, '20. No root folder in cases  (' || strCaseRef || ')');
      END IF;

      -- End
      INSERT INTO GEDProcessing (ProcessedDate,
                                 ProcessedBy,
                                 XMLParameter,
                                 FilePath,
                                 FileName,
                                 FileDate,
                                 CaseReference,
                                 CaseKey,
                                 ErrorMessage)
           VALUES (SYSDATE,
                   psUserIdentityKey,
                   psXmlParameters,
                   strFilePath,
                   strFileName,
                   strFileDate,
                   strCaseRef,
                   intCaseKey,
                   NULL);

      OPEN pcResult FOR
         SELECT strFilePath AS FilePath,
                strFileName AS FileName,
                TO_CHAR (intCaseKey, 'FM9999999999999') AS CaseKey,
                TO_CHAR (intFolderKey, 'FM9999999999999') AS FolderKey,
                NULL AS ErrorMessage
           FROM DUAL;
   EXCEPTION
      WHEN OTHERS
      THEN
         OracleErrorCode := SQLCODE;
         OracleErrorMessage := SUBSTR (SQLERRM (OracleErrorCode), 1, 1024);

         INSERT INTO GEDProcessing (ProcessedDate,
                                    ProcessedBy,
                                    XMLParameter,
                                    FilePath,
                                    FileName,
                                    FileDate,
                                    CaseReference,
                                    CaseKey,
                                    ErrorMessage)
              VALUES (SYSDATE,
                      psUserIdentityKey,
                      psXmlParameters,
                      strFilePath,
                      strFileName,
                      strFileDate,
                      strCaseRef,
                      intCaseKey,
                      OracleErrorMessage);

         OPEN pcResult FOR
            SELECT strFilePath AS FilePath,
                   strFileName AS FileName,
                   TO_CHAR (intCaseKey, 'FM9999999999999') AS CaseKey,
                   TO_CHAR (intFolderKey, 'FM9999999999999') AS FolderKey,
                   OracleErrorMessage AS ErrorMessage
              FROM DUAL;
   END;
END TOTALGED;
/