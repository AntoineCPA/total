﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CPA.EMEA.TOTAL.DocDispatch.BU
{
    public static class LocalSetting
    {
        public static string GetValue(string key, string defaultValue = "")
        {
            string retour = System.Configuration.ConfigurationManager.AppSettings.Get(key);

            if (string.IsNullOrEmpty(retour))
                return defaultValue;

            return retour;

        }

        public const string WS_USERIDENTITYKEY = "1";

        static public void WritMsg(string txt, params string[] tabs)
        {
            Console.WriteLine(txt, tabs);
        }

        public static string GetNextKeyPrefix()
        {
            return CPA.Common.DataTools.NextKeyPrefix;
         }

    }
}
