<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" encoding="utf-8" indent="yes"/>
	<xsl:template match="/">
			<xsl:apply-templates select="//TransactionBody"/>
	</xsl:template>
	<xsl:template match="//TransactionBody">
		<xsl:element name="CaseDataRaw" namespace="http://tempuri.org/CaseDataRaw.xsd">
			<xsl:element name="Case">
				<xsl:element name="ShortTitle"><xsl:value-of select="//DescriptionDetails[DescriptionCode='Short Title']/DescriptionText"/></xsl:element>
			</xsl:element>
			<xsl:for-each select="//IdentifierNumberDetails">
				<xsl:choose>
					<xsl:when test="./IdentifierNumberCode='Application'">
						<xsl:element name="CaseEvent" namespace="">
							<xsl:element name="EventKey">10</xsl:element>
							<xsl:element name="EventNumber"><xsl:value-of select="./IdentifierNumberText"/></xsl:element>
							<xsl:element name="EventDate"><xsl:value-of select="//EventDetails[EventCode='Application']/EventDate"/></xsl:element>
						</xsl:element>
					</xsl:when>
					<xsl:when test="./IdentifierNumberCode='Publication'">
						<xsl:element name="CaseEvent" namespace="">
							<xsl:element name="EventKey">15</xsl:element>
							<xsl:element name="EventNumber"><xsl:value-of select="./IdentifierNumberText"/></xsl:element>
							<xsl:element name="EventDate"><xsl:value-of select="//EventDetails[EventCode='Publication']/EventDate"/></xsl:element>
						</xsl:element>
					</xsl:when>
					<xsl:when test="./IdentifierNumberCode='Registration/Grant'">
						<xsl:element name="CaseEvent" namespace="">
							<xsl:element name="EventKey">25</xsl:element>
							<xsl:element name="EventNumber"><xsl:value-of select="./IdentifierNumberText"/></xsl:element>
							<xsl:element name="EventDate"><xsl:value-of select="//EventDetails[EventCode='Registration/Grant']/EventDate"/></xsl:element>
						</xsl:element>
					</xsl:when>
				</xsl:choose>
			</xsl:for-each>
			<xsl:element name="USProsecution">
				<xsl:element name="ArtUnit"><xsl:value-of select="//IdentifierNumberDetails[IdentifierNumberCode='Group Art Unit']/IdentifierNumberText"/></xsl:element>
				<xsl:element name="EventNumber"><xsl:value-of select="//IdentifierNumberDetails[IdentifierNumberCode='Customer Number']/IdentifierNumberText"/></xsl:element>
				<xsl:element name="Examinator"><xsl:value-of select="//NameDetails[NameTypeCode='Examiner']/AddressBook/FormattedNameAddress/Name/FormattedName/LastName"/></xsl:element>
			</xsl:element>
			<xsl:element name="CaseName">
				<xsl:element name="Name"><xsl:value-of select="//NameDetails[NameTypeCode='Inventor']/AddressBook/FormattedNameAddress/Name/FormattedName/LastName"/></xsl:element>
			</xsl:element>
			<xsl:element name="CaseClass">
				<xsl:element name="ClassCode"><xsl:value-of select="//GoodsServicesDetails/ClassDescriptionDetails/ClassDescription/ClassNumber"/></xsl:element>
				<xsl:element name="GoodsInEnglish"><xsl:value-of select="//GoodsServicesDetails/ClassificationTypeCode"/></xsl:element>
			</xsl:element>
			<xsl:for-each select="//EventDetails[EventText='Transaction History Event']">
			<xsl:element name="TransactionHistory">
				<xsl:element name="TransactionDate"><xsl:value-of select="./EventDate"/></xsl:element>
				<xsl:element name="TransactionDescription"><xsl:value-of select="./EventCode"/></xsl:element>
			</xsl:element>
			</xsl:for-each>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
