<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
  <xsl:import href="../StandardLayout.xslt" />
  
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:param name="ApplicationPath" select="'http://localhost/CPAmemotech'"/>
  <xsl:param name="ServerAppPath" select="'c:\inetpub\wwwroot\CPAmemotech'"/>
  <xsl:param name="Orientation" select="'Portrait'"/>
  <xsl:param name="PaperSize" select="'A4'"/>

  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
      <!-- layout master set -->
	    <xsl:call-template name="FOFileHeader"/>

      <!-- DOCUMENT RENDERING -->
      <fo:page-sequence master-reference="pages" font-family="Verdana" font-size="8pt" initial-page-number="1">
        <!-- Region Before/After/firstAfter -->
        <xsl:call-template name="RegionBefore"/>
        <xsl:call-template name="RegionAfter"/>
        <xsl:call-template name="FirstRegionAfter"/>
        <!-- End Region Before/After/firstAfter -->
        
        <!-- BODY -->
        <fo:flow flow-name="xsl-region-body">
          <!-- Criteria and Freetext -->
		      <xsl:apply-templates select="//CriteriaZone"/>
		      <xsl:apply-templates select="//FreeText"/>
		      <!-- Criteria and Freetext -->

          <fo:block >
          <!-- Place here your report XSL-FO -->
          
             <fo:block font-weight="bold"  text-decoration="underline">
              <xsl:value-of select="//Localization/InvoiceListeTitle"/>
            </fo:block>
            <fo:block>
              <fo:table width="100%" padding-top="10px">
                <fo:table-column column-width="proportional-column-width(20)"/>
                <fo:table-column column-width="proportional-column-width(10)"/>
                <fo:table-column column-width="proportional-column-width(30)"/>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/AgentName"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/AgentCode"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/FormattedAddress"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    
                    <xsl:for-each select="//AgentList">
                      <fo:table-row>
                      <fo:table-cell text-align="left" padding-left="5px" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./Name"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="5px" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./NameCode"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="5px" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./FormattedAddress"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    </xsl:for-each>
                  </fo:table-body>
              </fo:table>
            </fo:block>
          
          
          
          
          
          
          
          </fo:block>
          <fo:block id="theEnd"/>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>
