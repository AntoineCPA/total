<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
  <xsl:import href="../StandardLayout.xslt" />
  
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:param name="ApplicationPath" select="'http://localhost/CPAmemotech'"/>
  <xsl:param name="ServerAppPath" select="'c:\inetpub\wwwroot\CPAmemotech'"/>
  <xsl:param name="Orientation" select="'Landscape'"/>
  <xsl:param name="PaperSize" select="'A4'"/>

  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
      <!-- layout master set -->
	    <xsl:call-template name="FOFileHeader"/>

      <!-- DOCUMENT RENDERING -->
      <fo:page-sequence master-reference="pages" font-family="Verdana" font-size="8pt" initial-page-number="1">
        <!-- Region Before/After/firstAfter -->
        <xsl:call-template name="RegionBefore"/>
        <xsl:call-template name="RegionAfter"/>
        <xsl:call-template name="FirstRegionAfter"/>
        <!-- End Region Before/After/firstAfter -->
        
        <!-- BODY -->
        <fo:flow flow-name="xsl-region-body">
          <!-- Criteria and Freetext -->
		      <xsl:apply-templates select="//CriteriaZone"/>
		      <xsl:apply-templates select="//FreeText"/>
		      <!-- Criteria and Freetext -->

          <fo:block >
          <!-- Place here your report XSL-FO -->
          
          <xsl:if test="count(//InvoiceList)>0">
            <fo:block font-weight="bold"  text-decoration="underline">
              <xsl:value-of select="//Localization/InvoiceListeTitle"/>
            </fo:block>
            <fo:block>
              <fo:table width="800px" padding-top="10px">
                <fo:table-column column-width="proportional-column-width(15)"/>
                <fo:table-column column-width="proportional-column-width(5)"/>
                <fo:table-column column-width="proportional-column-width(3)"/>
                <fo:table-column column-width="proportional-column-width(7)"/>
                <fo:table-column column-width="proportional-column-width(50)"/>
                <fo:table-body>
                    <fo:table-row>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/InvoiceNumber"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/Amount"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/CurrencyCode"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/InvoiceLineNumber"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                    <xsl:for-each select="//InvoiceList">
                      <fo:table-row>
                      <fo:table-cell text-align="left" padding-left="5px" font-weight="bold" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./InvoiceNumber"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="right" padding-right="5px" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./Amount"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./CurrencyCode"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./InvoiceLineNumber"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="5px">
                        <fo:block>
                          <xsl:value-of select="./ErrorDisplayed"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    </xsl:for-each>
                  </fo:table-body>
              </fo:table>
            </fo:block>
          </xsl:if> 
           
          <xsl:if test="count(//InvoiceError)>0">
            <fo:block font-weight="bold"  text-decoration="underline" padding-top="20px">
              <xsl:value-of select="//Localization/InvoiceErrorTitle"/>
            </fo:block>
            <fo:block padding-top="10px">
              <fo:table width="100%">
                <fo:table-column column-width="proportional-column-width(10)"/>
                <fo:table-column column-width="proportional-column-width(5)"/>
                <fo:table-column column-width="proportional-column-width(7)"/>
                <fo:table-column column-width="proportional-column-width(20)"/>
                <fo:table-column column-width="proportional-column-width(7)"/>
                <fo:table-column column-width="proportional-column-width(5)"/>
                <fo:table-column column-width="proportional-column-width(10)"/>
                <fo:table-column column-width="proportional-column-width(5)"/>
                <fo:table-column column-width="proportional-column-width(25)"/>
                <fo:table-header>
                    <fo:table-row>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/InvoiceNumber"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/ItemNumber"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/Division"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/ClientReference"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/RenewalDate"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/AnnuityNumber"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/Amount"/>
                            </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/CurrencyCode"/>
                            </fo:block>
                        </fo:table-cell>
                    </fo:table-row>
                </fo:table-header>
                <fo:table-body>
                    <xsl:for-each select="//InvoiceError">
                      <fo:table-row>
                      <fo:table-cell text-align="left" padding-left="5px" font-weight="bold" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./InvoiceNumber"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./InvoiceItem"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="5px" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./DivisionCode"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="5px" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./ClientReference"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="5px" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./RenewalDate"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./RenewalNumber"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="right" padding-right="5px" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./Amount"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="./Currency"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="5px">
                        <fo:block>
                          <xsl:value-of select="./ErrorDisplayed"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    </xsl:for-each>
                     
                  </fo:table-body>
              </fo:table>
            </fo:block>
          </xsl:if>


            <xsl:if test="count(//CreditNote)>0">
              <fo:block font-weight="bold"  text-decoration="underline" padding-top="20px">
                <xsl:value-of select="//Localization/CreditNoteTitle"/>
              </fo:block>
              <fo:block padding-top="5px">
                <xsl:value-of select="//Localization/CreditNoteExplaination"/>
              </fo:block>
              <fo:block padding-top="10px">
                <fo:table width="100%">
                  <fo:table-column column-width="proportional-column-width(20)"/>
                  <fo:table-column column-width="proportional-column-width(5)"/>
                  <fo:table-column column-width="proportional-column-width(5)"/>
                  <fo:table-column column-width="proportional-column-width(3)"/>
                  <fo:table-column column-width="proportional-column-width(8)"/>
                  <fo:table-column column-width="proportional-column-width(7)"/>
                  <fo:table-column column-width="proportional-column-width(7)"/>
                  <fo:table-column column-width="proportional-column-width(5)"/>
                  <fo:table-column column-width="proportional-column-width(10)"/>
                  <fo:table-column column-width="proportional-column-width(5)"/>
                  <fo:table-header>
                    <fo:table-row>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/ClientReference"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/CEFNumber"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/EventDate"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/EventCode"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/InvoiceNumber"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/Division"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/RenewalDate"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/AnnuityNumber"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/Amount"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                        <fo:block>
                          <xsl:value-of select="//Localization/CurrencyCode"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                  </fo:table-header>
                  <fo:table-body>
                    <xsl:for-each select="//CreditNote">
                      <fo:table-row>
                        <fo:table-cell text-align="left" padding-left="5px" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./ClientReference"/>
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./CompositeEventNumber"/>
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./EventDate"/>
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./EventCode"/>
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="left" padding-left="5px" font-weight="bold" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./InvoiceNumber"/>
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="left" padding-left="5px" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./DivisionCode"/>
                          </fo:block>
                        </fo:table-cell>

                        <fo:table-cell text-align="left" padding-left="5px" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./RenewalDate"/>
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./RenewalNumber"/>
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="right" padding-right="5px" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./Amount"/>
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="center" border="solid black 0.5pt">
                          <fo:block>
                            <xsl:value-of select="./Currency"/>
                          </fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                    </xsl:for-each>

                  </fo:table-body>
                </fo:table>
              </fo:block>
            </xsl:if>


          </fo:block>
          <fo:block id="theEnd"/>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>
