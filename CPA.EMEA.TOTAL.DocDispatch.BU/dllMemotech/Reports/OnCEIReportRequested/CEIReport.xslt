<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
  <xsl:import href="../StandardLayout.xslt" />
  
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:param name="ApplicationPath" select="'http://localhost/CPAmemotech'"/>
  <xsl:param name="ServerAppPath" select="'c:\inetpub\wwwroot\CPAmemotech'"/>
  <xsl:param name="Orientation" select="'Landscape'"/>
  <xsl:param name="PaperSize" select="'A4'"/>
  <xsl:param name="Why_Cases_Extracted" select="'False'"/>

  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
      <!-- layout master set -->
	    <xsl:call-template name="FOFileHeader"/>
      <!-- DOCUMENT RENDERING -->
      <fo:page-sequence master-reference="pages" font-family="Verdana" font-size="8pt" initial-page-number="1">
        <!-- Region Before/After/firstAfter -->
        <xsl:call-template name="RegionBefore"/>
        <xsl:call-template name="RegionAfterCEI"/>
        <xsl:call-template name="FirstRegionAfter"/>
        <!-- End Region Before/After/firstAfter -->
        
        <!-- BODY -->
        <fo:flow flow-name="xsl-region-body">
          <!-- Criteria and Freetext -->
		      <xsl:apply-templates select="//CriteriaZone"/>
		      <xsl:apply-templates select="//FreeText"/>
		      <!-- Criteria and Freetext -->

          <fo:block >
          <!-- Place here your report XSL-FO -->
          
            <fo:block >
              <fo:table width="800px"  table-layout="fixed">
                <fo:table-column column-width="proportional-column-width(12)"/>
                <fo:table-column column-width="proportional-column-width(4)"/>
                <fo:table-body>
                
                  <!-- Extraction-->
                  <xsl:if test="//ActionReportParameter[ParamName='TypeOfRequest' and ParamValue='0'] or //ActionReportParameter[ParamName='TypeOfRequest' and ParamValue='1']">
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="//Localization/LastExtractionDate"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                           <xsl:value-of select="//Summary/LastExtractionDate"/>
                         </fo:block>
                       </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                          <xsl:value-of select="//Localization/CEIAgentName"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                          <xsl:value-of select="//Summary/CEIAgentName"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="//Localization/SystemId"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                           <xsl:value-of select="//Summary/SystemId"/>
                         </fo:block>
                       </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="//Localization/ClientNumber"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                           <xsl:value-of select="//Summary/ClientNumber"/>
                         </fo:block>
                       </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="//Localization/ExtractMode"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                          <xsl:if test="//Summary/ExtractMode='0'"><xsl:value-of select="//Localization/strModeNormal"/>
                          </xsl:if>
                          <xsl:if test="//Summary/ExtractMode='1'"><xsl:value-of select="//Localization/strModeTest"/>
                          </xsl:if>
                         </fo:block>
                       </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="//Localization/BatchDate"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                           <xsl:value-of select="//Summary/BatchDate"/>
                         </fo:block>
                       </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="//Localization/BatchNumber"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                           <xsl:value-of select="//Summary/BatchNumber"/>
                         </fo:block>
                       </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="//Localization/NbrPatent"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                           <xsl:value-of select="//Summary/NbrPatent"/>
                         </fo:block>
                       </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="//Localization/NbrTrademark"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                           <xsl:value-of select="//Summary/NbrTrademark"/>
                         </fo:block>
                       </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="//Localization/NbrDesign"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                           <xsl:value-of select="//Summary/NbrDesign"/>
                         </fo:block>
                       </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                          <xsl:value-of select="//Localization/NbrError"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                          <xsl:value-of select="//Summary/NbrError"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    
                  </xsl:if>
                  
                  <!--Other than Extraction (Standard report)-->
                  <xsl:if test="not(//ActionReportParameter[ParamName='TypeOfRequest' and ParamValue='0'] or //ActionReportParameter[ParamName='TypeOfRequest' and ParamValue='1'])">
                    <xsl:for-each select="//Summary">
                      <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                           <xsl:value-of select="./CEIMessage"/>
                         </fo:block>
                       </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                           <xsl:value-of select="./ClientValue"/>
                         </fo:block>
                       </fo:table-cell>
                      </fo:table-row>
                    </xsl:for-each>
                    
                    <fo:table-row>
                      <fo:table-cell padding-top="1px" >
                          <fo:block>
                            &#160;
                          </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    
                    <!--<fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                          <xsl:value-of select="//Localization/SystemId"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                          <xsl:value-of select="//GlobalSetting/SYSTEMID"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    <fo:table-row>
                      <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                          <xsl:value-of select="//Localization/ClientNumber"/>
                        </fo:block>
                      </fo:table-cell>
                      <fo:table-cell text-align="left" padding-left="10px">
                        <fo:block>
                          <xsl:value-of select="//GlobalSetting/CLIENTNUMBER"/>
                        </fo:block>
                      </fo:table-cell>
                    </fo:table-row>-->
                  </xsl:if>
                  
                  
                  <!--<fo:table-row>
                    <fo:table-cell text-align="left" font-weight="bold">
                      <fo:block>
                         <xsl:value-of select="//Localization/NbrError"/>
                       </fo:block>
                     </fo:table-cell>
                    <fo:table-cell text-align="left" padding-left="10px">
                      <fo:block>
                         <xsl:value-of select="count(//Error/CeiReportKey)"/>
                       </fo:block>
                     </fo:table-cell>
                  </fo:table-row>-->
                  
                  <fo:table-row>
                     <fo:table-cell padding-top="30px" >
                        <fo:block>
                          &#160;
                         </fo:block>
                     </fo:table-cell>
                   </fo:table-row>


                   <xsl:if test="$Why_Cases_Extracted='False'">
                     <xsl:if test="count(//Comparison)>0">
                       <xsl:for-each select="//Comparison/Narrative[not(.=../following-sibling::Comparison[1]/Narrative)]">  
                        <xsl:variable name="Narrative" select="."/>
                          <fo:table-row>
                            <fo:table-cell text-align="left" font-weight="bold">
                                <fo:block>
                                  <xsl:value-of select="."/>
                                </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="left" padding-left="10px">
                                <fo:block>
                                  <xsl:value-of select="count(//Comparison[Narrative=$Narrative])"/>
                                </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                        </xsl:for-each>
                        
                        <fo:table-row>
                          <fo:table-cell padding-top="3px" >
                            <fo:block>
                              &#160;
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                        
                        <fo:table-row>
                          <fo:table-cell text-align="left" font-weight="bold" border-top="solid black 0.5pt">
                              <fo:block>
                                <xsl:value-of select="//Localization/Total"/>
                              </fo:block>
                          </fo:table-cell>
                          <fo:table-cell text-align="left" border-top="solid black 0.5pt" padding-left="10px">
                              <fo:block>
                                <xsl:value-of select="count(//Comparison)"/>
                              </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </xsl:if>
                    </xsl:if>

                    <xsl:if test="$Why_Cases_Extracted='True'">
                      <xsl:if test="count(//WhyCasesExtracted)>0">
                        <xsl:for-each select="//WhyCasesExtracted/Narrative[not(.=../following-sibling::WhyCasesExtracted[1]/Narrative)]">
                          <xsl:variable name="Narrative" select="."/>
                          <fo:table-row>
                            <fo:table-cell text-align="left" font-weight="bold">
                              <fo:block>
                                <xsl:value-of select="."/>
                              </fo:block>
                            </fo:table-cell>
                            <fo:table-cell text-align="left" padding-left="10px">
                              <fo:block>
                                <xsl:value-of select="count(//WhyCasesExtracted[Narrative=$Narrative])"/>
                              </fo:block>
                            </fo:table-cell>
                          </fo:table-row>
                        </xsl:for-each>

                        <fo:table-row>
                          <fo:table-cell padding-top="3px" >
                            <fo:block>
                              &#160;
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>

                        <fo:table-row>
                          <fo:table-cell text-align="left" font-weight="bold" border-top="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="//Localization/Total"/>
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell text-align="left" border-top="solid black 0.5pt" padding-left="10px">
                            <fo:block>
                              <xsl:value-of select="count(//WhyCasesExtracted)"/>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </xsl:if>
                    </xsl:if>
                  
                  </fo:table-body>
               </fo:table>
             </fo:block>
             
             <!--Composite Event-->
             <xsl:if test="//ActionReportParameter[ParamName='TypeOfRequest' and ParamValue='4']">
               <fo:block >
                <fo:table width="460px" table-layout="fixed">
                  <fo:table-column column-width="proportional-column-width(10)"/>
                  <fo:table-column column-width="proportional-column-width(4)"/>
                  <fo:table-body>
                    <fo:table-row keep-with-next="always">
                      <fo:table-cell padding-top="3px" >
                          <fo:block>
                            &#160;
                          </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                    
                    <fo:table-row keep-with-next="always">
                     <fo:table-cell text-align="left" font-weight="bold">
                        <fo:block>
                          <xsl:value-of select="//Localization/ImportedEvent"/>
                         </fo:block>
                     </fo:table-cell>
                    </fo:table-row> 
                    
                    <fo:table-row keep-with-next="always">
                      <fo:table-cell padding-top="1px" >
                          <fo:block>
                            &#160;
                          </fo:block>
                      </fo:table-cell>
                    </fo:table-row>
                  
                    <xsl:for-each select="//CompositeEventList">
                      <fo:table-row keep-with-next="always">
                        <fo:table-cell text-align="left" font-weight="bold">
                          <fo:block>
                            <xsl:value-of select="./EventDescription"/>
                          </fo:block>
                        </fo:table-cell>
                        <fo:table-cell text-align="left" padding-left="10px">
                          <fo:block>
                            <xsl:value-of select="./Count"/>
                          </fo:block>
                        </fo:table-cell>
                      </fo:table-row>
                    </xsl:for-each>
                     
                     
                    </fo:table-body>
                 </fo:table>
               </fo:block>
             </xsl:if>

             <xsl:if test="$Why_Cases_Extracted='False'">
               <xsl:if test="count(//Error)>0">
                <fo:block break-after="page"/>
                <fo:block font-size="10pt" padding-bottom="5px">
                  <xsl:value-of select="//Localization/OracleMessage"/>
                </fo:block>
                <fo:block >
                  <fo:table width="100%" border="solid black 0.5pt" table-layout="fixed">
                    <fo:table-column column-width="proportional-column-width(1)"/>
                    <fo:table-column column-width="proportional-column-width(3)"/>
                    <fo:table-column column-width="proportional-column-width(1)"/>
                    <fo:table-column column-width="proportional-column-width(1)"/>
                    <fo:table-column column-width="proportional-column-width(4)"/>
                    
                   
                    <fo:table-header>
                      <fo:table-row>
                           <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                              <fo:block>
                                 <xsl:value-of select="//Localization/CaseCodeW"/>
                               </fo:block>
                           </fo:table-cell>
                           <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                              <fo:block>
                                 <xsl:value-of select="//Localization/CaseReference"/>
                               </fo:block>
                           </fo:table-cell>
                           <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                              <fo:block>
                                 <xsl:value-of select="//Localization/LineNumber"/>
                               </fo:block>
                           </fo:table-cell>
                           <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                              <fo:block>
                                 <xsl:value-of select="//Localization/OracleNumber"/>
                               </fo:block>
                           </fo:table-cell>
                           <fo:table-cell text-align="center" font-weight="bold" background-color="#EFE7F7" color="#22145C" border="solid black 0.5pt">
                              <fo:block>
                                 <xsl:value-of select="//Localization/ErrorMessage"/>
                               </fo:block>
                           </fo:table-cell>
                      </fo:table-row>
                    </fo:table-header>
      
                    <fo:table-body>
                      <xsl:for-each select="//Error">
                        <fo:table-row>
                          <fo:table-cell padding-left="3pt" text-align="left" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="./CaseKey"/>
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell padding-left="3pt" text-align="left" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="./ClientReference"/>
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell text-align="center" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="./LineNumber"/>
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell text-align="center" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="./OracleNumber"/>
                            </fo:block>
                          </fo:table-cell>
                          <fo:table-cell padding-left="3pt" text-align="left" border="solid black 0.5pt">
                            <fo:block>
                              <xsl:value-of select="./OracleMessage"/>
                            </fo:block>
                          </fo:table-cell>
                        </fo:table-row>
                      </xsl:for-each>
                    </fo:table-body>
                  </fo:table>
                </fo:block>
              </xsl:if>
             </xsl:if>

           </fo:block>
          <fo:block id="theEnd"/>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>

  <xsl:template name="RegionAfterCEI">
    <fo:static-content flow-name="xsl-region-after" font-size="8pt">
      <fo:block>
        <fo:table width="100%" table-layout="fixed">
          <fo:table-column column-width="proportional-column-width(1)"/>
          <fo:table-column column-width="proportional-column-width(1)"/>
          <fo:table-column column-width="proportional-column-width(1)"/>
          <fo:table-body>

            <xsl:if test="$Why_Cases_Extracted='True'">
              <!-- Extraction Normal and Anomaly Normal-->
              <xsl:if test="//ActionReportParameter[ParamName='TypeOfRequest' and ParamValue='0'] or //ActionReportParameter[ParamName='TypeOfRequest' and ParamValue='2']">
                <fo:table-row display-align="center">
                  <fo:table-cell>
                    <fo:block>
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell padding-bottom="10px">
                    <fo:block text-align="left">
                      <xsl:value-of select="//Localization/LegendImported"/>
                    </fo:block>
                    <fo:block text-align="left">
                      <xsl:value-of select="//Localization/LegendExtracted"/>
                    </fo:block>
                  </fo:table-cell>
                  <fo:table-cell text-align="end">
                    <fo:block>
                    </fo:block>
                  </fo:table-cell>
                </fo:table-row>
              </xsl:if>
            </xsl:if>


              <fo:table-row display-align="center">
              <fo:table-cell>
                <fo:block>
                  <xsl:value-of select="//RegionAfter/ProductName"/>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell>
                <fo:block text-align="center" >
                  <xsl:value-of select="//RegionAfter/ClientCompanyName"/>
                </fo:block>
              </fo:table-cell>
              <fo:table-cell text-align="end">
                <fo:block>
                  <xsl:value-of select="//Localization/PrintedOn"/>&#160;<xsl:value-of select="//RegionAfter/PrintDate"/>
                </fo:block>
              </fo:table-cell>
            </fo:table-row>
          </fo:table-body>
        </fo:table>
      </fo:block>
    </fo:static-content>
  </xsl:template>
</xsl:stylesheet>
