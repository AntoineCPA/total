<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
  <xsl:import href="../StandardLayout.xslt"/>
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
  <xsl:param name="ServerAppPath" select="'c:\inetpub\wwwroot\CPAmemotech'"/>
  <xsl:param name="Orientation" select="'Portrait'"/>
  <xsl:param name="PaperSize" select="'A4'"/>
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
      <!-- layout master set -->
        <xsl:call-template name="FOFileHeader"/>
<!--
      <fo:layout-master-set>
			<fo:simple-page-master master-name="firstpage" margin-right="0.2cm" margin-left="0.2cm" margin-bottom="0.5cm" margin-top="0.5cm">
				<fo:region-body margin-top="0cm" margin-bottom="0cm" margin-right="0cm" margin-left="0cm"/>
				<fo:region-before extent="0cm"/>
				<fo:region-after region-name="firstfooter" extent="0cm"/>
			</fo:simple-page-master>
			<fo:simple-page-master master-name="page" margin-right="0.5cm" margin-left="0.5cm" margin-bottom="0.5cm" margin-top="0.5cm">
				<fo:region-body margin-top="0cm" margin-bottom="0cm" margin-right="0cm" margin-left="0cm"/>
				<fo:region-before extent="0cm"/>
				<fo:region-after extent="0cm"/>
			</fo:simple-page-master>
			<fo:page-sequence-master master-name="pages">
				<fo:repeatable-page-master-alternatives>
					<fo:conditional-page-master-reference master-reference="page" page-position="rest"/>
					<fo:conditional-page-master-reference master-reference="firstpage" page-position="first"/>
				</fo:repeatable-page-master-alternatives>
			</fo:page-sequence-master>
		  </fo:layout-master-set>-->
      <!-- DOCUMENT RENDERING -->
      <fo:page-sequence master-reference="pages" font-family="Verdana" font-size="6pt" initial-page-number="1">
        <!-- Region Before/After/firstAfter -->
        <xsl:call-template name="aRegionBefore"/>
        <xsl:call-template name="RegionAfter"/>
        <xsl:call-template name="FirstRegionAfter"/>
        <!-- End Region Before/After/firstAfter -->
        
        <!-- BODY -->
        <fo:flow flow-name="xsl-region-body">
        <fo:block font-size="10pt" font-weight="bold" padding-bottom="10pt">
			TABLEAU DE BORD: <xsl:value-of select="//ReportInfo/CurrentMonthName"/>&#160;<xsl:value-of select="//ReportInfo/CurrentYear"/>
        </fo:block>
          <fo:block>
             <!-- Tableau 1-->
							<fo:table width="100%" table-layout="fixed" border="0.5pt #CECFCE">
								<fo:table-column column-width="proportional-column-width(1.0)" />
    						<fo:table-column column-width="proportional-column-width(1.2)" />
								<fo:table-column column-width="proportional-column-width(0.9)" />
								<fo:table-column column-width="proportional-column-width(1.2)" />
								<fo:table-column column-width="proportional-column-width(0.9)" />
								<fo:table-column column-width="proportional-column-width(0.9)" />
								<fo:table-column column-width="proportional-column-width(1)" />
								<fo:table-column column-width="proportional-column-width(1.2)" />
								<fo:table-column column-width="proportional-column-width(0.7)" />
								<fo:table-column column-width="proportional-column-width(0.6)" />
								<fo:table-column column-width="proportional-column-width(1.3)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.2)" />
								<fo:table-column column-width="proportional-column-width(0.9)" />
								<fo:table-header>
									<fo:table-row>
										<fo:table-cell padding-left="3pt" number-columns-spanned="14" font-weight="bold" background-color="#EFE7F7" color="#22145C">
											<fo:block>
												Nombre de premiers Dépôts par Centre de Résultat et Type (dépôt, co-dépôt, gestionnaires)
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row color="#22145C" font-style="italic">
  									<fo:table-cell number-rows-spanned="2" text-align="center" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="left" number-columns-spanned="7" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt" padding-left="1pt">
											<fo:block>
												Premiers dépôts FRANCE hors Soleau et BNI
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="left" border="solid #CECFCE 0.5pt" number-columns-spanned="2" border-right="solid #CECFCE 1.5pt" padding-left="1pt">
											<fo:block>
												Dépôts FRANCE
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-rows-spanned="2" text-align="center"  background-color="#DFDFDF" font-weight="bold" border="solid #CECFCE 0.5pt"  >
											<fo:block>
												Total France (Total2+H+I) (J)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-rows-spanned="2" text-align="center"  border="solid #CECFCE 0.5pt" >
											<fo:block>
											1er dépôts étranger* (K)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-rows-spanned="2" text-align="center" background-color="#DFDFDF" font-weight="bold" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Total 1er dépôts (Total1 + K) (L)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell number-rows-spanned="2" text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												France BN hors  1er dépôt (M)
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row color="#22145C" font-style="italic" border-bottom="solid #CECFCE 0.5">
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Gérant IFP Dépôt IFP seul (A)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Gérant IFP Codépôt (B)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Gérant X (co)dépôt IFP (C)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" background-color="#E7E7E7" font-weight="bold" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
												Total 1 (A+B+C) (D)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" padding-left="1pt">
											<fo:block>
												Gérant X Dépôt X (E)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Gérant IFP Dépôt X (F)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center"  background-color="#DFDFDF" font-weight="bold" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
												Total 2  (Total1+E+F) (G)
											</fo:block>
										</fo:table-cell>
											<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												SOLEAU (H)
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt" padding-left="3pt" padding-right="3pt">
											<fo:block>
												BNI (I)
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>
									  <xsl:apply-templates select="//FirstFlgByBU"/> 
								</fo:table-body>
							</fo:table>
							<fo:table width="100%" table-layout="fixed">
							<fo:table-column column-width="proportional-column-width(5)" />
								<fo:table-body>
									<fo:table-row>
										<fo:table-cell>
											<fo:block>
													(*) 1er dépôts étrangers sur lesquels l'IFP a des droits (gérant ou (co)déposant) 
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-body>
							</fo:table>
							<fo:block padding-top="15pt"/>
							 <!-- Tableau 2-->
							<fo:table width="100%" table-layout="fixed" border="0.5pt #CECFCE">
								<fo:table-column column-width="proportional-column-width(2.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-header>
								<fo:table-row>
										<fo:table-cell padding-left="3pt" number-columns-spanned="9" font-weight="bold" background-color="#EFE7F7" color="#22145C">
											<fo:block>
												Nombre de premiers Dépôts par Ingénieurs Brevet
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row color="#22145C" font-style="italic">
									<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" number-rows-spanned="2">
											<fo:block>
												1er Dépôts par ingénieurs
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" font-weight="bold" border="solid #CECFCE 0.5pt"  number-columns-spanned="2">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" font-weight="bold" border="solid #CECFCE 0.5pt" number-columns-spanned="2">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear1"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center"  font-weight="bold" border="solid #CECFCE 0.5pt" number-columns-spanned="2">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear2"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center"  font-weight="bold" border="solid #CECFCE 0.5pt" number-columns-spanned="2">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear3"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row color="#22145C" font-style="italic" border-bottom="solid #CECFCE 0.5">
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Tout
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												1er Dépôts
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Tout
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												1er Dépôts
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Tout
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												1er Dépôts
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Tout
											</fo:block>
										</fo:table-cell>
											<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												1er Dépôts
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>
									   <xsl:apply-templates select="//FirstFlgByAttorney"/> 
								</fo:table-body>
							</fo:table>								
							<fo:block padding-top="15pt"/>
							 <!-- Tableau 3-->
							<fo:table width="100%" table-layout="fixed" border="0.5pt solid #CECFCE">
								<fo:table-column column-width="proportional-column-width(1.5)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-header>
								<fo:table-row>
										<fo:table-cell padding-left="3pt" number-columns-spanned="25" font-weight="bold" background-color="#EFE7F7" color="#22145C">
											<fo:block>
												Nombre de Dépôts par Centre de Résultat (Business Unit) et pour tout l'IFP
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row color="#22145C" font-style="italic" border-bottom="solid #CECFCE 0.5">
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" number-rows-spanned="2" border-right="solid #CECFCE 1.5pt">
											<fo:block>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center"  border="solid #CECFCE 0.5pt" number-columns-spanned="3" border-right="solid #CECFCE 1.5pt">
											<fo:block font-weight="bold">
												Dépôts BN 
											</fo:block>
											<fo:block font-weight="bold">
												 Etrangers
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt"  number-columns-spanned="3" border-right="solid #CECFCE 1.5pt">
											<fo:block font-weight="bold" >
												Dépôts EP
											</fo:block>
											<fo:block>
												 Pays EP voie OEB
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt"  number-columns-spanned="3" border-right="solid #CECFCE 1.5pt">
											<fo:block font-weight="bold" >
												Dépôts PCT
											</fo:block>
											<fo:block>
												 Pays WP voie PCT
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt"  number-columns-spanned="3" border-right="solid #CECFCE 1.5pt">
											<fo:block font-weight="bold" >
												Délivrances EP
											</fo:block>
											<fo:block>
												 Phases nationales
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt"  number-columns-spanned="3" border-right="solid #CECFCE 1.5pt">
											<fo:block font-weight="bold" >
												Phases Nationales
											</fo:block>
											<fo:block>
												 Type PCT
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt"  number-columns-spanned="3" border-right="solid #CECFCE 1.5pt">
											<fo:block font-weight="bold" >
												Dépôts ECT
											</fo:block>
											<fo:block>
												 Pays EP voie ECT
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt"  number-columns-spanned="3" border-right="solid #CECFCE 1.5pt">
											<fo:block font-weight="bold" >
												Dépôts FRANCE
											</fo:block>
											<fo:block>
												 Sauf EP et Soleau
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt"  number-columns-spanned="3">
											<fo:block font-weight="bold" >
												TOTAL
											</fo:block>
											<fo:block>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row color="#22145C" font-style="italic" border-bottom="solid #CECFCE 0.5">>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear1"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear2"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear1"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear2"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear1"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear2"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear1"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear2"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear1"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear2"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear1"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear2"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear1"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" border-right="solid #CECFCE 1.5pt">
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear2"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear1"/>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt" >
											<fo:block>
												<xsl:value-of select="//ReportInfo/CurrentYear2"/>
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>
									   <xsl:apply-templates select="//FilingByTypeAndBU"/> 
								</fo:table-body>
							</fo:table>
							<fo:block padding-top="15pt"/>
							 <!-- Tableau 4-->
							<fo:table width="100%" table-layout="fixed" border="0.5pt solid #CECFCE">
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-column column-width="proportional-column-width(1.0)" />
								<fo:table-header>
								<fo:table-row>
										<fo:table-cell padding-left="3pt" number-columns-spanned="10" font-weight="bold" background-color="#EFE7F7" color="#22145C">
											<fo:block>
												Nombre de brevets vivants, morts par Centre de Résultat
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
									<fo:table-row color="#22145C" font-style="italic" border-bottom="solid #CECFCE 0.5">
									<fo:table-cell text-align="center" border="solid #CECFCE 0.5pt">
											<fo:block>
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" font-weight="bold" border="solid #CECFCE 0.5pt" padding-left="1pt">
											<fo:block>
												Nb Brevets Total
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" background-color="#E7E7E7" font-weight="bold" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Nb Brevets Vivants
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" font-weight="bold" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Nb Brevets Morts
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" font-weight="bold" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Nb Dossiers Total
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" font-weight="bold" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Nb Dossiers Vivants
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" background-color="#E7E7E7" font-weight="bold" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Abandon - Cessions
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" background-color="#E7E7E7" font-weight="bold" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Expiré
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" font-weight="bold" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Age moyen Vivants
											</fo:block>
										</fo:table-cell>
										<fo:table-cell text-align="center" font-weight="bold" border="solid #CECFCE 0.5pt" >
											<fo:block>
												Age moyen Morts
											</fo:block>
										</fo:table-cell>
									</fo:table-row>
								</fo:table-header>
								<fo:table-body>
									   <xsl:apply-templates select="//LiveAndDeadPatentByBU"/> 
								</fo:table-body>
							</fo:table>								
							<fo:block padding-top="15pt"/>
              <xsl:if test="position()!=last()">
                <fo:block break-after="page"/>
              </xsl:if>
          </fo:block>
          <fo:block id="theEnd"/>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
  <!-- FirstFlgByBU  TEMPLATE -->
	<xsl:template match="FirstFlgByBU">
		<fo:table-row>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="left" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./DESCRIPTION"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./GERANTIFPDEPOTIFP"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./GERANTIFPCODEPOTIFP"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./GERANTXCODEPOTIFP"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" background-color="#E7E7E7" font-weight="bold" text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./TOTAL1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./GERANTXDEPOTX"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./GERANTIFPDEPOTX"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" background-color="#E7E7E7" font-weight="bold" text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./TOTAL2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./DEPOTFRSOLEAU"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./DEPOTFRBNI"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  background-color="#E7E7E7" font-weight="bold" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./TOTALFR"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./DEPOTNONFR"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" background-color="#E7E7E7" font-weight="bold" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./TOTALPREMDEPOT"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./DEPOTFRHORSPREM"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- FirstFlgByAttorney  TEMPLATE -->
	<xsl:template match="FirstFlgByAttorney">
		<fo:table-row>
			<fo:table-cell  border="solid #CECFCE 0.5pt"  text-align="left" >
				<fo:block>
					<xsl:value-of select="./DESCRIPTION"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell  border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt" >
				<fo:block>
					<xsl:value-of select="./ALLFIRSTFLGY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./FIRSTFLGY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./ALLFIRSTFLGY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./FIRSTFLGY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./ALLFIRSTFLGY2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./FIRSTFLGY2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./ALLFIRSTFLGY3"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./FIRSTFLGY3"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<!-- FilingByTypeAndBU  TEMPLATE -->
	<xsl:template match="FilingByTypeAndBU">
		<fo:table-row>
			<fo:table-cell  border="solid #CECFCE 0.5pt"  text-align="left" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./DESCRIPTION"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell  border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt" >
				<fo:block>
					<xsl:value-of select="./FOREIGNY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./FOREIGNY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./FOREIGNY2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./EPY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./EPY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./EPY2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./PCTY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./PCTY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./PCTY2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./DELIVEPNATPHASEY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./DELIVEPNATPHASEY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./DELIVEPNATPHASEY2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./DELIVPCTNATPHASEY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./DELIVPCTNATPHASEY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./DELIVPCTNATPHASEY2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./EPTY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./EPTY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./EPTY2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./FRY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./FRY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt" border-right="solid #CECFCE 1.5pt">
				<fo:block>
					<xsl:value-of select="./FRY2"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./TOTALY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./TOTALY1"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./TOTALY2"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
		<!-- LiveAndDeadPatentByBU  TEMPLATE -->
	<xsl:template match="LiveAndDeadPatentByBU">
		<fo:table-row>
			<fo:table-cell  border="solid #CECFCE 0.5pt"  text-align="left" >
				<fo:block>
					<xsl:value-of select="./DESCRIPTION"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell  border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt" >
				<fo:block>
					<xsl:value-of select="./NBTOTPATENT"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell  border="solid #CECFCE 0.5pt" background-color="#E7E7E7" text-align="right" padding-right="1pt" >
				<fo:block>
					<xsl:value-of select="./NBLIVEPATENT"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./NBDEADPATENT"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./NBTOTFAMILY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./NBLIVEFAMILY"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  background-color="#E7E7E7" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./NBABANDPATENT"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt"  background-color="#E7E7E7" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./NBEXPIRPATENT"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./NBLIVEAVERAGE"/>
				</fo:block>
			</fo:table-cell>
			<fo:table-cell border="solid #CECFCE 0.5pt" text-align="right" padding-right="1pt">
				<fo:block>
					<xsl:value-of select="./NBDEADAVERAGE"/>
				</fo:block>
			</fo:table-cell>
		</fo:table-row>
	</xsl:template>
	<xsl:template name="aRegionBefore">
		<xsl:param name="RefId" select="'theEnd'"/>
		<fo:static-content flow-name="xsl-region-before" font-size="8pt">
			<fo:block>
				<fo:table width="100%" table-layout="fixed">
					<fo:table-column column-width="proportional-column-width(25)"/>
					<fo:table-column column-width="proportional-column-width(50)"/>
					<fo:table-column column-width="proportional-column-width(25)"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block>
									<xsl:if test="//RegionBefore/ClientCompanyLogoPath">
										<fo:external-graphic>
											<xsl:attribute name="src"><xsl:value-of select="//RegionBefore/ClientCompanyLogoPath"/></xsl:attribute>
										</fo:external-graphic>
									</xsl:if>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell display-align="center" text-align="center">
								<fo:block font-weight="bold" font-size="15pt">
									<xsl:value-of select="//RegionBefore/Title"/>
								</fo:block>
								<fo:block font-weight="bold" font-size="12pt">
									<xsl:value-of select="//RegionBefore/SubTitle"/>
								</fo:block>
								<fo:block font-weight="bold" font-size="10pt">
									<xsl:value-of select="//RegionBefore/BoldLine"/>
								</fo:block>
								<xsl:if test="string-length(//RegionBefore/PeriodFrom) &gt; 0">
  								  <fo:block font-size="10pt">
  								    <xsl:value-of select="//Localization/PeriodFrom"/>&#160;<xsl:value-of   select="//RegionBefore/PeriodFrom"/>&#160;
  								    <xsl:value-of select="//Localization/PeriodTo"/>&#160;<xsl:value-of   select="//RegionBefore/PeriodTo"/>
  								  </fo:block>
								</xsl:if>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:static-content>
	</xsl:template>
</xsl:stylesheet>
