<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
  <xsl:import href="../StandardLayout.xslt" />
  
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
  
  <xsl:param name="ServerAppPath" select="'c:\inetpub\wwwroot\CPAmemotech'"/>
  <xsl:param name="ApplicationPath" select="'C:\Program Files\CPA Software Solutions\Windows Services'"/>
  <xsl:param name="Orientation" select="'Portrait'"/>
  <xsl:param name="PaperSize" select="'A4'"/>
  
  <xsl:template match="/">
    <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" xmlns:fox="http://www.chive.com/apoc/ext">
      <!-- layout master set -->
	    <xsl:call-template name="FOFileHeader"/>

      <!-- DOCUMENT RENDERING -->
      <fo:page-sequence master-reference="pages" font-family="Verdana" font-size="8pt" initial-page-number="1">
        <!-- Region Before/After/firstAfter -->
        <xsl:call-template name="RegionBefore"/>
        <xsl:call-template name="RegionAfter"/>
        <xsl:call-template name="FirstRegionAfter"/>
        <!-- End Region Before/After/firstAfter -->
        
        <!-- BODY -->
        <fo:flow flow-name="xsl-region-body">
          <!-- Criteria and Freetext -->
		      <xsl:apply-templates select="//CriteriaZone"/>
		      <xsl:apply-templates select="//FreeText"/>
		      <!-- Criteria and Freetext -->

          <fo:block >
            <fo:block font-size="12pt" color="#22145C">Job Information</fo:block>
            <fo:block>This document logs the results of the job <fo:inline font-weight="bold"><xsl:value-of select="//JobInfo/JobDescription" /></fo:inline>.</fo:block>
              
            <fo:table width="100%" padding="3pt" padding-top="10pt">
              <fo:table-column column-width="proportional-column-width(20)"/>
              <fo:table-column column-width="proportional-column-width(80)"/>
              <fo:table-body>
                <fo:table-row>
                  <fo:table-cell color="#22145C"><fo:block>Job Name</fo:block></fo:table-cell>
                  <fo:table-cell><fo:block><xsl:value-of select="//JobInfo/JobDescription" /></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell color="#22145C"><fo:block>Job ID</fo:block></fo:table-cell>
                  <fo:table-cell><fo:block><xsl:value-of select="//JobInfo/JobKey" /></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell color="#22145C"><fo:block>Execution Schema</fo:block></fo:table-cell>
                  <fo:table-cell><fo:block><xsl:value-of select="//JobInfo/SchemaOwner" /></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell color="#22145C"><fo:block>Process ID</fo:block></fo:table-cell>
                  <fo:table-cell><fo:block><xsl:value-of select="//JobInfo/ProcessKey" /></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row >
                  <fo:table-cell padding-top="6pt" color="#22145C"><fo:block>Started on</fo:block></fo:table-cell>
                  <fo:table-cell padding-top="6pt"><fo:block><xsl:value-of select="//JobInfo/StartDate" /></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell color="#22145C"><fo:block>Ended on</fo:block></fo:table-cell>
                  <fo:table-cell><fo:block><xsl:value-of select="//JobInfo/EndDate" /></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row>
                  <fo:table-cell color="#22145C"><fo:block>Total time</fo:block></fo:table-cell>
                  <fo:table-cell><fo:block><xsl:value-of select="//JobInfo/TotalTime" /></fo:block></fo:table-cell>
                </fo:table-row>
                <fo:table-row >
                  <fo:table-cell padding-top="6pt" color="#22145C"><fo:block>Result</fo:block></fo:table-cell>
                  <fo:table-cell padding-top="6pt" ><fo:block font-weight="bold">
                    <xsl:if test="//JobInfo/HasError=0">No Error</xsl:if>
                    <xsl:if test="//JobInfo/HasError=1">Errors encountered</xsl:if>
                  </fo:block></fo:table-cell>
                </fo:table-row>
              </fo:table-body>
            </fo:table>
            
            <fo:block font-size="12pt" color="#22145C" padding-top="10pt">Events Information</fo:block>
            <fo:table width="100%" padding="3pt" >
              <fo:table-column column-width="proportional-column-width(5)"/>
              <fo:table-column column-width="proportional-column-width(12)"/>
              <fo:table-column column-width="proportional-column-width(13)"/>
              <xsl:if test="//JobInfo/JobKey=8">
                <fo:table-column column-width="proportional-column-width(14)"/>
              </xsl:if>
              <fo:table-column column-width="proportional-column-width(70)"/>
              <fo:table-header>
                <fo:table-row>
                  <fo:table-cell />
                  <fo:table-cell><fo:block color="#22145C">Date</fo:block></fo:table-cell>
                  <fo:table-cell><fo:block color="#22145C">Time</fo:block></fo:table-cell>
                  <xsl:if test="//JobInfo/JobKey=8">
                    <fo:table-cell><fo:block color="#22145C">Elapsed Time</fo:block></fo:table-cell>
                  </xsl:if>
                  <fo:table-cell><fo:block color="#22145C">Description</fo:block></fo:table-cell>
                </fo:table-row>
              </fo:table-header>
              <fo:table-body>
                <xsl:for-each select="//EventsInfo">
                  <fo:table-row>
                    <fo:table-cell>
                      <fo:block>
                        <xsl:if test="HasError=1">
                          <fo:external-graphic src="{$ApplicationPath}\Reports\images\icon.critical.png" height="10px" width="10px" />
                        </xsl:if>
                      </fo:block>
                    </fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="LogDate" /></fo:block></fo:table-cell>
                    <fo:table-cell><fo:block><xsl:value-of select="LogTime" /></fo:block></fo:table-cell>
                    <xsl:if test="//JobInfo/JobKey=8">
                      <fo:table-cell><fo:block><xsl:value-of select="ElapsedTime" /></fo:block></fo:table-cell>
                    </xsl:if>                    
                    <fo:table-cell>
                      <fo:block><xsl:value-of select="Description" /></fo:block>
                      <xsl:if test="NativeErrorText">
                        <fo:block  margin-left="10pt" color="silver"><xsl:value-of select="NativeErrorText" /></fo:block>
                      </xsl:if>
                    </fo:table-cell>
                  </fo:table-row>
                </xsl:for-each>
              </fo:table-body>
            </fo:table>
          </fo:block>
          <fo:block id="theEnd"/>
        </fo:flow>
      </fo:page-sequence>
    </fo:root>
  </xsl:template>
</xsl:stylesheet>
