<?xml version="1.0" encoding="utf-8"?>

<!--StandardFormAttachment stylesheet:
    Given any data set and a table in that dataset, returns a simple html representation of that table.-->
<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="TableName"/>

  <xsl:template match="/">
    <html>
      <body>
        <table>
          <xsl:apply-templates select="//*[name() = $TableName]"/>
        </table>
      </body>
    </html>
  </xsl:template>

  <!--Every element gives rise to a column; elements added systematicaly by fm_GetFormDetailForPrint are not shown-->
  <xsl:template match="*">
    <!--Use node names in place of column titles-->
    <xsl:if test="position() = 1">
      <tr>
        <xsl:for-each select="./*[name() != 'VALIDFORMPARAMETER_TYPEOFUSE' and name() != 'FORM_PARAMETERTYPE' and name() != 'FORM_PARAMETERDESCRIPTION' and name() != 'FIELD_SEPARATOR']">
          <td>
            <xsl:value-of select="name()"/>
          </td>
        </xsl:for-each>
      </tr>
    </xsl:if>
    <tr>
      <xsl:for-each select="./*[name() != 'VALIDFORMPARAMETER_TYPEOFUSE' and name() != 'FORM_PARAMETERTYPE' and name() != 'FORM_PARAMETERDESCRIPTION' and name() != 'FIELD_SEPARATOR']">
        <td>
          <xsl:value-of select="."/>
        </td>
      </xsl:for-each>
    </tr>
  </xsl:template>
  
</xsl:stylesheet> 

