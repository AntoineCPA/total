<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:fo="http://www.w3.org/1999/XSL/Format">
	<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>
	
	<!-- Standard paper size in accordance with the paper format: A4 or us-letter 
	       NB: the size is defined for the orientation 'Portrait'
	-->
	<xsl:variable name="PaperSizeWidth">
	  <xsl:if test="$PaperSize='US-Letter'"><xsl:value-of select="string(21.59)" /></xsl:if>
	  <xsl:if test="not($PaperSize='US-Letter')"><xsl:value-of select="string(21.0)" /></xsl:if>
	</xsl:variable>
	
	<xsl:variable name="PaperSizeHeight">
	  <xsl:if test="$PaperSize='US-Letter'"><xsl:value-of select="string(27.94)" /></xsl:if>
	  <xsl:if test="not($PaperSize='US-Letter')"><xsl:value-of select="string(29.7)" /></xsl:if>
	</xsl:variable>
	
	<!-- Set the document size in accordance with the paper orientation -->
	<xsl:variable name="PageWidth">
	  <xsl:if test="$Orientation='Landscape'"><xsl:value-of select="$PaperSizeHeight" /></xsl:if>
	  <xsl:if test="not($Orientation='Landscape')"><xsl:value-of select="$PaperSizeWidth" /></xsl:if>
	</xsl:variable >
	
	<xsl:variable name="PageHeight" >
	  <xsl:if test="not($Orientation='Landscape')"><xsl:value-of select="$PaperSizeHeight" /></xsl:if>
	  <xsl:if test="$Orientation='Landscape'"><xsl:value-of select="$PaperSizeWidth" /></xsl:if>
	</xsl:variable >
	
	<!-- Standard template for the page layout -->
	<xsl:template name="FOFileHeader">
		<fo:layout-master-set>
			<fo:simple-page-master master-name="firstpage" margin-right="0.5cm" margin-left="0.5cm" margin-bottom="0.5cm" margin-top="0.5cm">
				<xsl:attribute name="page-width"><xsl:value-of select="$PageWidth"/>cm</xsl:attribute>
				<xsl:attribute name="page-height"><xsl:value-of select="$PageHeight"/>cm</xsl:attribute>
				<fo:region-body margin-top="2.5cm" margin-bottom="2.5cm" margin-right="0cm" margin-left="0cm"/>
				<fo:region-before extent="2.5cm"/>
				<fo:region-after region-name="firstfooter" extent="2.0cm"/>
			</fo:simple-page-master>
			<fo:simple-page-master master-name="page" margin-right="0.5cm" margin-left="0.5cm" margin-bottom="0.5cm" margin-top="0.5cm">
				<xsl:attribute name="page-width"><xsl:value-of select="$PageWidth"/>cm</xsl:attribute>
				<xsl:attribute name="page-height"><xsl:value-of select="$PageHeight"/>cm</xsl:attribute>
				<fo:region-body margin-top="2.5cm" margin-bottom="2.5cm" margin-right="0cm" margin-left="0cm"/>
				<fo:region-before extent="2.5cm"/>
				<fo:region-after extent="2.0cm"/>
			</fo:simple-page-master>
			<fo:page-sequence-master master-name="pages">
				<fo:repeatable-page-master-alternatives>
					<fo:conditional-page-master-reference master-reference="page" page-position="rest"/>
					<fo:conditional-page-master-reference master-reference="firstpage" page-position="first"/>
				</fo:repeatable-page-master-alternatives>
			</fo:page-sequence-master>
		</fo:layout-master-set>
	</xsl:template>
	
	<xsl:template name="RegionBefore">
		<xsl:param name="RefId" select="'theEnd'"/>
		<fo:static-content flow-name="xsl-region-before" font-size="8pt">
			<fo:block>
				<fo:table width="100%" table-layout="fixed">
					<fo:table-column column-width="proportional-column-width(25)"/>
					<fo:table-column column-width="proportional-column-width(50)"/>
					<fo:table-column column-width="proportional-column-width(25)"/>
					<fo:table-body>
						<fo:table-row>
							<fo:table-cell>
								<fo:block>
									<xsl:if test="//RegionBefore/ClientCompanyLogoPath">
										<fo:external-graphic>
											<xsl:attribute name="src"><xsl:value-of select="//RegionBefore/ClientCompanyLogoPath"/></xsl:attribute>
										</fo:external-graphic>
									</xsl:if>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell display-align="center" text-align="center">
								<fo:block font-weight="bold" font-size="15pt">
									<xsl:value-of select="//RegionBefore/Title"/>
								</fo:block>
								<fo:block font-weight="bold" font-size="12pt">
									<xsl:value-of select="//RegionBefore/SubTitle"/>
								</fo:block>
								<fo:block font-weight="bold" font-size="10pt">
									<xsl:value-of select="//RegionBefore/BoldLine"/>
								</fo:block>
								<xsl:if test="string-length(//RegionBefore/PeriodFrom) &gt; 0">
  								  <fo:block font-size="10pt">
  								    <xsl:value-of select="//Localization/PeriodFrom"/>&#160;<xsl:value-of   select="//RegionBefore/PeriodFrom"/>&#160;
  								    <xsl:value-of select="//Localization/PeriodTo"/>&#160;<xsl:value-of   select="//RegionBefore/PeriodTo"/>
  								  </fo:block>
								</xsl:if>
							</fo:table-cell>
							<fo:table-cell display-align="center">
								<fo:block text-align="right">
									<xsl:value-of select="//Localization/Page"/>&#160;<fo:page-number/>&#160;
									<xsl:value-of select="//Localization/PageSeparator"/>&#160;
									<fo:page-number-citation>
										<xsl:attribute name="ref-id"><xsl:value-of select="$RefId"/></xsl:attribute>
									</fo:page-number-citation>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:static-content>
	</xsl:template>
	
	<xsl:template name="FirstRegionAfter">
		<fo:static-content flow-name="firstfooter" font-size="8pt">
			<fo:block>
				<fo:table width="100%" table-layout="fixed">
					<fo:table-column column-width="proportional-column-width(1)"/>
					<fo:table-column column-width="proportional-column-width(1)"/>
					<fo:table-column column-width="proportional-column-width(1)"/>
					<fo:table-body>
						<fo:table-row display-align="center">
							<fo:table-cell>
								<fo:block>
									<xsl:value-of select="//RegionAfter/ProductName"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block text-align="center">
									<xsl:value-of select="//RegionAfter/ClientCompanyName"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="end">
								<fo:block>
									<xsl:value-of select="//Localization/PrintedOn"/>&#160;<xsl:value-of select="//RegionAfter/PrintDate"/>
								</fo:block>
								<fo:block>
									<xsl:value-of select="//Localization/PrintedBy"/>&#160;<xsl:value-of select="//RegionAfter/PrintBy"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:static-content>
	</xsl:template>
	
	<xsl:template name="RegionAfter">
		<fo:static-content flow-name="xsl-region-after" font-size="8pt">
			<fo:block>
				<fo:table width="100%" table-layout="fixed">
					<fo:table-column column-width="proportional-column-width(1)"/>
					<fo:table-column column-width="proportional-column-width(1)"/>
					<fo:table-column column-width="proportional-column-width(1)"/>
					<fo:table-body>
						<fo:table-row display-align="center">
							<fo:table-cell>
								<fo:block>
									<xsl:value-of select="//RegionAfter/ProductName"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell>
								<fo:block text-align="center">
									<xsl:value-of select="//RegionAfter/ClientCompanyName"/>
								</fo:block>
							</fo:table-cell>
							<fo:table-cell text-align="end">
								<fo:block>
									<xsl:value-of select="//Localization/PrintedOn"/>&#160;<xsl:value-of select="//RegionAfter/PrintDate"/>
								</fo:block>
							</fo:table-cell>
						</fo:table-row>
					</fo:table-body>
				</fo:table>
			</fo:block>
		</fo:static-content>
	</xsl:template>
	<xsl:template match="Line">
		<fo:block text-indent="5pt">
			<xsl:value-of select="."/>
		</fo:block>
	</xsl:template>
	
	<xsl:template match="CriteriaZone">
		<fo:block border="solid #CCCC99 0.5pt" space-after="5pt">
			<fo:block text-indent="5pt" font-weight="bold" font-size="10pt" background-color="#F2F2E7" color="#22145C" border="inherit">
				<xsl:value-of select="//Localization/Criteria"/>
			</fo:block>
			<fo:block padding="5pt">
				<xsl:choose>
					<xsl:when test="@Mode='Bullets'">
						<fo:list-block>
							<xsl:for-each select="./Line">
								<fo:list-item>
									<fo:list-item-label start-indent="15pt">
										<fo:block>
											<fo:external-graphic width="6px">
												<xsl:attribute name="src"><xsl:value-of select="//CriteriaZone/BulletImagePath"/></xsl:attribute>
											</fo:external-graphic>
										</fo:block>
									</fo:list-item-label>
									<fo:list-item-body start-indent="25pt">
										<fo:block>
											<xsl:value-of select="."/>
										</fo:block>
									</fo:list-item-body>
								</fo:list-item>
							</xsl:for-each>
						</fo:list-block>
					</xsl:when>
					<xsl:otherwise>
						<fo:block>
							<xsl:apply-templates/>
						</fo:block>
					</xsl:otherwise>
				</xsl:choose>
			</fo:block>
		</fo:block>
	</xsl:template>
	<xsl:template match="FreeText">
		<fo:block space-after="5pt" space-before="5pt">
			<xsl:apply-templates/>
		</fo:block>
	</xsl:template>
</xsl:stylesheet>
