﻿namespace CPA.EMEA.TOTAL.DocDispatch.TestWF
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnStart = new System.Windows.Forms.Button();
            this.btnLookForDirectory = new System.Windows.Forms.Button();
            this.tbxResult = new System.Windows.Forms.TextBox();
            this.tbxExempleFileName = new System.Windows.Forms.TextBox();
            this.btnCreateDirectory = new System.Windows.Forms.Button();
            this.btnDBAccessTest = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.btnDMSAccess = new System.Windows.Forms.Button();
            this.btnUploadFile = new System.Windows.Forms.Button();
            this.btnGetCaseFolderKey = new System.Windows.Forms.Button();
            this.btnTestTimer = new System.Windows.Forms.Button();
            this.btnTestGetFilePathFromFileName = new System.Windows.Forms.Button();
            this.tbxFileName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(12, 152);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(189, 71);
            this.btnStart.TabIndex = 0;
            this.btnStart.Text = "btnStartMoulinette";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // btnLookForDirectory
            // 
            this.btnLookForDirectory.Location = new System.Drawing.Point(418, 31);
            this.btnLookForDirectory.Name = "btnLookForDirectory";
            this.btnLookForDirectory.Size = new System.Drawing.Size(160, 38);
            this.btnLookForDirectory.TabIndex = 1;
            this.btnLookForDirectory.Text = "btnLookForDirectory";
            this.btnLookForDirectory.UseVisualStyleBackColor = true;
            this.btnLookForDirectory.Click += new System.EventHandler(this.btnLookForDirectory_Click);
            // 
            // tbxResult
            // 
            this.tbxResult.Location = new System.Drawing.Point(227, 152);
            this.tbxResult.Multiline = true;
            this.tbxResult.Name = "tbxResult";
            this.tbxResult.ReadOnly = true;
            this.tbxResult.Size = new System.Drawing.Size(777, 524);
            this.tbxResult.TabIndex = 2;
            // 
            // tbxExempleFileName
            // 
            this.tbxExempleFileName.Location = new System.Drawing.Point(748, 39);
            this.tbxExempleFileName.Name = "tbxExempleFileName";
            this.tbxExempleFileName.Size = new System.Drawing.Size(256, 22);
            this.tbxExempleFileName.TabIndex = 3;
            this.tbxExempleFileName.Text = "BIO10-10-EP";
            // 
            // btnCreateDirectory
            // 
            this.btnCreateDirectory.Location = new System.Drawing.Point(584, 31);
            this.btnCreateDirectory.Name = "btnCreateDirectory";
            this.btnCreateDirectory.Size = new System.Drawing.Size(158, 38);
            this.btnCreateDirectory.TabIndex = 4;
            this.btnCreateDirectory.Text = "btnCreateDirectory";
            this.btnCreateDirectory.UseVisualStyleBackColor = true;
            this.btnCreateDirectory.Click += new System.EventHandler(this.btnCreateDirectory_Click);
            // 
            // btnDBAccessTest
            // 
            this.btnDBAccessTest.Location = new System.Drawing.Point(12, 389);
            this.btnDBAccessTest.Name = "btnDBAccessTest";
            this.btnDBAccessTest.Size = new System.Drawing.Size(189, 45);
            this.btnDBAccessTest.TabIndex = 5;
            this.btnDBAccessTest.Text = "btnDBAccessTest";
            this.btnDBAccessTest.UseVisualStyleBackColor = true;
            this.btnDBAccessTest.Click += new System.EventHandler(this.btnDBAccessTest_Click);
            // 
            // btnTest
            // 
            this.btnTest.Location = new System.Drawing.Point(12, 229);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(189, 67);
            this.btnTest.TabIndex = 6;
            this.btnTest.Text = "btnTest";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // btnDMSAccess
            // 
            this.btnDMSAccess.Location = new System.Drawing.Point(12, 460);
            this.btnDMSAccess.Name = "btnDMSAccess";
            this.btnDMSAccess.Size = new System.Drawing.Size(189, 45);
            this.btnDMSAccess.TabIndex = 7;
            this.btnDMSAccess.Text = "btnDMSAccess";
            this.btnDMSAccess.UseVisualStyleBackColor = true;
            this.btnDMSAccess.Click += new System.EventHandler(this.btnDMSAccess_Click);
            // 
            // btnUploadFile
            // 
            this.btnUploadFile.Location = new System.Drawing.Point(12, 540);
            this.btnUploadFile.Name = "btnUploadFile";
            this.btnUploadFile.Size = new System.Drawing.Size(189, 45);
            this.btnUploadFile.TabIndex = 8;
            this.btnUploadFile.Text = "btnUploadFile";
            this.btnUploadFile.UseVisualStyleBackColor = true;
            this.btnUploadFile.Click += new System.EventHandler(this.btnUploadFile_Click);
            // 
            // btnGetCaseFolderKey
            // 
            this.btnGetCaseFolderKey.Location = new System.Drawing.Point(152, 31);
            this.btnGetCaseFolderKey.Name = "btnGetCaseFolderKey";
            this.btnGetCaseFolderKey.Size = new System.Drawing.Size(260, 38);
            this.btnGetCaseFolderKey.TabIndex = 9;
            this.btnGetCaseFolderKey.Text = "btnGetCaseFolderKey";
            this.btnGetCaseFolderKey.UseVisualStyleBackColor = true;
            this.btnGetCaseFolderKey.Click += new System.EventHandler(this.btnGetCaseFolderKey_Click);
            // 
            // btnTestTimer
            // 
            this.btnTestTimer.Location = new System.Drawing.Point(12, 631);
            this.btnTestTimer.Name = "btnTestTimer";
            this.btnTestTimer.Size = new System.Drawing.Size(189, 45);
            this.btnTestTimer.TabIndex = 10;
            this.btnTestTimer.Text = "btnTestTimer";
            this.btnTestTimer.UseVisualStyleBackColor = true;
            this.btnTestTimer.Click += new System.EventHandler(this.btnTestTimer_Click);
            // 
            // btnTestGetFilePathFromFileName
            // 
            this.btnTestGetFilePathFromFileName.Location = new System.Drawing.Point(12, 90);
            this.btnTestGetFilePathFromFileName.Name = "btnTestGetFilePathFromFileName";
            this.btnTestGetFilePathFromFileName.Size = new System.Drawing.Size(319, 44);
            this.btnTestGetFilePathFromFileName.TabIndex = 11;
            this.btnTestGetFilePathFromFileName.Text = "btnTestGetFilePathFromFileName";
            this.btnTestGetFilePathFromFileName.UseVisualStyleBackColor = true;
            this.btnTestGetFilePathFromFileName.Click += new System.EventHandler(this.btnTestGetFilePathFromFileName_Click);
            // 
            // tbxFileName
            // 
            this.tbxFileName.Location = new System.Drawing.Point(360, 104);
            this.tbxFileName.Name = "tbxFileName";
            this.tbxFileName.Size = new System.Drawing.Size(644, 22);
            this.tbxFileName.TabIndex = 12;
            this.tbxFileName.Text = "20161020-NNNNNNNNNNNNNNN-F-1318-EP-EPA-non du fichier.docx";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1029, 698);
            this.Controls.Add(this.tbxFileName);
            this.Controls.Add(this.btnTestGetFilePathFromFileName);
            this.Controls.Add(this.btnTestTimer);
            this.Controls.Add(this.btnGetCaseFolderKey);
            this.Controls.Add(this.btnUploadFile);
            this.Controls.Add(this.btnDMSAccess);
            this.Controls.Add(this.btnTest);
            this.Controls.Add(this.btnDBAccessTest);
            this.Controls.Add(this.btnCreateDirectory);
            this.Controls.Add(this.tbxExempleFileName);
            this.Controls.Add(this.tbxResult);
            this.Controls.Add(this.btnLookForDirectory);
            this.Controls.Add(this.btnStart);
            this.Name = "Form1";
            this.Text = "WFTestTotal";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.Button btnLookForDirectory;
        private System.Windows.Forms.TextBox tbxResult;
        private System.Windows.Forms.TextBox tbxExempleFileName;
        private System.Windows.Forms.Button btnCreateDirectory;
        private System.Windows.Forms.Button btnDBAccessTest;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.Button btnDMSAccess;
        private System.Windows.Forms.Button btnUploadFile;
        private System.Windows.Forms.Button btnGetCaseFolderKey;
        private System.Windows.Forms.Button btnTestTimer;
        private System.Windows.Forms.Button btnTestGetFilePathFromFileName;
        private System.Windows.Forms.TextBox tbxFileName;
    }
}

