﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using CPA.EMEA.TOTAL.DocDispatch.BU;

using System.Collections.ObjectModel;
using Cpa.Dms.Contracts;
using Cpa.Dms.Contracts.Data  ;
using Cpa.Dms.Contracts.Services ;

using System.IO;
using System.Transactions;

namespace CPA.EMEA.TOTAL.DocDispatch.TestWF
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            try
            {
                BU.Business bu = new Business();
                bu.msgCusto += WriteMsg;

                bu.Moulinette();

                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                WriteMsg("Error : {0}", ex.Message);
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
            }
        }

        private void btnLookForDirectory_Click(object sender, EventArgs e)
        {


            //J:\Professional_Services\Memotech web\Total > Refining & Chemical\Project Management\
            WriteMsg(new CPA.EMEA.TOTAL.DocDispatch.BU.DataAccessHelper().GetFilePathFromFileName(tbxExempleFileName.Text, "date"));

        }

        private void WriteMsg(string msg, params object[] param)
        {
            if (param == null || param.Count() == 0)
            {
                msg = msg.Replace("{", "{{").Replace("}", "}}");
            }
            tbxResult.Text += string.Format(msg, param) + "\r\n";
            tbxResult.Update();

            //if (!string.IsNullOrEmpty(CPA.Loreal.Data.LocalSetting.GetValue("pathNameLogFile", string.Empty)))
            //{
            //    System.IO.File.AppendAllText(CPA.Loreal.Data.LocalSetting.GetValue("pathNameLogFile"), DateTime.Now.ToString("yyMMdd-HH:mm:ss") + "\t" + string.Format(msg, param) + "\r\n");
            //}
        }

        private void btnCreateDirectory_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            try
            {
                CPA.EMEA.TOTAL.DocDispatch.BU.Business oBusiness = new CPA.EMEA.TOTAL.DocDispatch.BU.Business();

                string fullDirectory = new CPA.EMEA.TOTAL.DocDispatch.BU.DataAccessHelper().GetFilePathFromFileName(tbxExempleFileName.Text, "date");
                //J:\Professional_Services\Memotech web\Total > Refining & Chemical\Project Management\
                WriteMsg("Create: {0}", fullDirectory);

                WriteMsg(oBusiness.CreateDirectories(fullDirectory));
                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                WriteMsg("Error : {0}", ex.Message);
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
            }

        }

        private void btnTest_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            try
            {
                string Inputdate = "20161020";

                DateTime dtFile = DateTime.ParseExact(Inputdate, @"yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);

                Inputdate = dtFile.ToString("yyyy-MM-dd");
                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                WriteMsg("Error : {0}", ex.Message);
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
            }

        }

        private void btnDBAccessTest_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            try
            {

                WriteMsg(@"Memotech date: {0}", new CPA.EMEA.TOTAL.DocDispatch.BU.DataAccessHelper().GetCurrentDate());

                WriteMsg(@"FileName : date: {0}", new CPA.EMEA.TOTAL.DocDispatch.BU.DataAccessHelper().GetFilePathFromFileName("FileName", "fileDate"));

                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                WriteMsg("Error : {0}", ex.Message);
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
            }

        }

        private void btnDMSAccess_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            try
            {
                using (DMSService service = new DMSService("ThirdPartyDMS"))
                {
                    service.Endpoint.EndpointBehaviors.Add(new MyBehavior());
                    service.Open();


                    System.Collections.ObjectModel.Collection<DmsFeature> temp = service.Operations().GetFeatures();

                    WriteMsg("DMS has {0} features", temp.Count);
                }

                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                WriteMsg("Error : {0}", ex.Message);
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
            }
        }

        private void btnUploadFile_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();

            string keyDocCreated;
            try
            {

                Cpa.Dms.Contracts.Data.Document tpDoc = new Cpa.Dms.Contracts.Data.Document();
                tpDoc.Title = "Titre du doc";

                tpDoc.Archived = false;
                tpDoc.Authors = "Jhon Doe";

                tpDoc.File = new Cpa.Dms.Contracts.Data.DocumentFile();

                tpDoc.File.FileName = "DocTest6.txt";
                tpDoc.File.MimeType = @"text/plain";

                string filepathname = @"C:\Temp\totaltemp\DocUplaoadDeTest.txt";
                using (FileStream file = new FileStream(filepathname, FileMode.Open, FileAccess.Read))
                {
                    tpDoc.File.BinaryContent = new byte[file.Length];
                    file.Read(tpDoc.File.BinaryContent, 0, (int)file.Length);

                }

                tpDoc.File.Size = tpDoc.File.BinaryContent.LongLength;
                tpDoc.Metadata = new System.Collections.ObjectModel.Collection<Cpa.Dms.Contracts.Data.DmsMetadata>();

                Cpa.Dms.Contracts.Data.DmsMetadata meta = new Cpa.Dms.Contracts.Data.DmsMetadata();
                meta.PropertyName = "MediaTypeKey";
                meta.PropertyValue = "550";
                tpDoc.Metadata.Add(meta);

                meta = new Cpa.Dms.Contracts.Data.DmsMetadata();
                meta.PropertyName = "IsUniqueMedia";
                meta.PropertyValue = true;
                tpDoc.Metadata.Add(meta);

                meta = new Cpa.Dms.Contracts.Data.DmsMetadata();
                meta.PropertyName = "FolderContentKey";
                meta.PropertyValue = "NewServerKey0000000016";
                tpDoc.Metadata.Add(meta);

                tpDoc.Reference = "LogFileTestDepuisCGG.txt";
                tpDoc.Subject = "";
                tpDoc.Comments = "";

                tpDoc.Answers = new System.Collections.ObjectModel.Collection<Cpa.Dms.Contracts.Data.Document>();
                tpDoc.Answers.Clear();

                tpDoc.OwnerEntity = new Cpa.Dms.Contracts.Data.BusinessEntity();
                // TODO casekey 
                // for F-1318  => 6299713
                tpDoc.OwnerEntity.EntityKey = @"6299713";

                tpDoc.Created = DateTime.Now;
                tpDoc.Modified = DateTime.Now;

                tpDoc.ParentFolder = new Cpa.Dms.Contracts.Data.Folder();

                // TODO folderkey  
                // foder  F-1318  => 23704301
                tpDoc.ParentFolder.FolderKey = @"23704301";
                tpDoc.DocumentKey = CPA.EMEA.TOTAL.DocDispatch.BU.LocalSetting.GetNextKeyPrefix();

                using (TransactionScope trScope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions(), EnterpriseServicesInteropOption.Automatic))
                {
                    using (DMSService service = new DMSService("ThirdPartyDMS"))
                    {
                        service.Endpoint.EndpointBehaviors.Add(new MyBehavior());
                        service.Open();

                        Cpa.Dms.Contracts.Data.Document tpDocRetour = service.Operations().AddDocument(tpDoc);

                        keyDocCreated = tpDocRetour.DocumentKey;
                    }

                    trScope.Complete();
                }



                 ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                WriteMsg("Error : {0}", ex.Message);
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
            }
        }

        private void btnGetCaseFolderKey_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();
            try
            {
                DataAccessHelper da = new DataAccessHelper();

                string[] result = da.GetCaseFolderKey(tbxExempleFileName.Text);

               WriteMsg($"Casekey:{result[0]}, FolderKey: {result[1]}");

                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                WriteMsg("Error : {0}", ex.Message);
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
            }
        }

        System.Timers.Timer aTimer;
        int i = 1;

        private void btnTestTimer_Click(object sender, EventArgs e)
        {

            // Normally, the timer is declared at the class level, so that it stays in scope as long as it
            // is needed. If the timer is declared in a long-running method, KeepAlive must be used to prevent
            // the JIT compiler from allowing aggressive garbage collection to occur before the method ends.
            // You can experiment with this by commenting out the class-level declaration and uncommenting 
            // the declaration below; then uncomment the GC.KeepAlive(aTimer) at the end of the method.        
            //System.Timers.Timer aTimer;

         

            // Create a timer and set a two second interval.
            aTimer = new System.Timers.Timer();
            aTimer.Interval = 2000;

            // Alternate method: create a Timer with an interval argument to the constructor.
            //aTimer = new System.Timers.Timer(2000);

            // Create a timer with a two second interval.
            aTimer = new System.Timers.Timer(2000);

            // Hook up the Elapsed event for the timer. 
            aTimer.Elapsed += OnTimedEvent;

            // Have the timer fire repeated events (true is the default)
            aTimer.AutoReset = true;
            

            // Start the timer
            aTimer.Enabled = true;

            

            WriteMsg("Press the Enter key to exit the program at any time... ");
            

        }

        private void OnTimedEvent(Object source, System.Timers.ElapsedEventArgs e)
        {
            // Have the timer fire repeated events (true is the default)
            aTimer.Stop();
            i++;
            string myid = DateTime.Now.Ticks.ToString() ;
            Random temp = new Random(DateTime.Now.Second);
            int hasToWait = 500 + temp.Next(0, 10000)  ;
            lock (this)
            { 
                System.IO.File.AppendAllText(@"C:\temp\testTimer.txt", DateTime.Now.ToString("mm:ss:fff") + "\t" + $"{i}, Start {myid.Substring(9)} event  at {e.SignalTime.ToString("mm: ss:fff")} waiting {hasToWait} " + "\r\n");
            }
            System.Threading.Thread.Sleep(hasToWait);

            lock (this)
            {
                System.IO.File.AppendAllText(@"C:\temp\testTimer.txt", DateTime.Now.ToString("mm:ss:fff") + "\t" + $"{i}, End {myid.Substring(9)} event  at {e.SignalTime.ToString("mm: ss:fff")}" + "\r\n");
            }


            aTimer.Start();
        }

        private void btnTestGetFilePathFromFileName_Click(object sender, EventArgs e)
        {
            ((Button)sender).BackColor = Color.Orange;
            ((Button)sender).Update();
            try
            {

                DataAccessHelper da = new DataAccessHelper();

                string date = tbxFileName.Text.Split('-')[0];

                

                DateTime dtFile = DateTime.ParseExact(date, @"yyyyMMdd", System.Globalization.CultureInfo.InvariantCulture);
                
                List<string> retour = da.GetFilePathFromFileName(tbxFileName.Text, dtFile);

                string newFilePath = retour[0];
                string newFileName = retour[1];
                string CaseKey = retour[2];
                string FolderKey = retour[3];
                string ErrorMessage = retour[4];

                if (!string.IsNullOrEmpty(ErrorMessage))
                {
                    throw new Exception(ErrorMessage);
                }

                WriteMsg($"newFilePath: \"{newFilePath}\"; \newFileName: \"{newFileName}\"; CaseKey: \"{CaseKey}\"; FolderKey: \"{FolderKey}\"; ErrorMessage: \"{ErrorMessage}\";");


                ((Button)sender).BackColor = Color.Green;
                ((Button)sender).Update();
            }
            catch (Exception ex)
            {
                WriteMsg("Error : {0}", ex.Message);
                ((Button)sender).BackColor = Color.Red;
                ((Button)sender).Update();
            }
        }

    }
}