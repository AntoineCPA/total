﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using CPA.EMEA.TOTAL.DocDispatch.BU;

namespace CPA.EMEA.TOTAL.DocDispatch
{
    public partial class DocDispatchWS : ServiceBase
    {
        System.Timers.Timer serviceTimer = new System.Timers.Timer();
        CPA.EMEA.TOTAL.DocDispatch.BU.Business OBu = null;

        public DocDispatchWS()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            DataAccessHelper.LogFile(MessageLevel.info, "OnStart windows Service");

            OBu = new CPA.EMEA.TOTAL.DocDispatch.BU.Business();

            DataAccessHelper.LogFile(MessageLevel.info, "OnStart inited BU.Business()");

            serviceTimer.Interval = double.Parse(CPA.EMEA.TOTAL.DocDispatch.BU.LocalSetting.GetValue("ProcessInterval"));

            // Have the timer fire repeated events (true is the default)
            serviceTimer.AutoReset = true;

            // Start the timer
            serviceTimer.Enabled = true;

            serviceTimer.Elapsed += Timer_Elapsed;
            serviceTimer.Start();
        }

        private void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            // The service is waiting between two process.
            serviceTimer.Stop();

            try
            {
                OBu.Moulinette();
            }
            catch (Exception ex)
            {
                DataAccessHelper.LogFile(MessageLevel.error, string.Format("Error in {0}, Msg : {1}.{2}", System.Reflection.MethodInfo.GetCurrentMethod(), ex.Message, ex.InnerException != null ? "\r\n InnerException: " + ex.InnerException : ""));
            }

            serviceTimer.Start();
        }

        protected override void OnStop()
        {
            serviceTimer.Stop();
            DataAccessHelper.LogFile(MessageLevel.info, "Stop windows Service");
        }
    }
}
